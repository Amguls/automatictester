<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="restapi.TestData" %>
<%@ page import="restapi.ResultData" %>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Submission</title>
	</head>
	<body>
		<h3>Submission Program</h3>
		<textarea cols="80" rows="15" style="resize:none" readonly><%= (String)request.getAttribute("submission") %></textarea>
		<h3>Test Results</h3>
		<table style="width:1200px; border-collapse: collapse; text-align:center;">
			<tr>
				<th>Test Name</th>
				<th>Test Input</th>
				<th>Submission Output</th>
				<th>Submission Result</th>
			</tr>
			<% List<TestData> tests = (List<TestData>)request.getAttribute("tests");
				Map<Integer, ResultData> results = (Map<Integer, ResultData>)request.getAttribute("results");
				for(TestData test : tests) {
					String outputMessage = results.get(test.testID).message;
					if (outputMessage.length() > 256) {
						outputMessage = outputMessage.substring(0, 256) + "... [some output hidden]";
					}
				%>
					<tr>
						<td style="border-bottom: 1px solid #ddd;">
							<b><%= test.name %></b>
						</td>
						<td style="border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;">
							<pre><%= new String(test.input.readAllBytes()) %></pre>
						</td>
						<td style="border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;">
							<pre style="text-align:left"><%= outputMessage %></pre>
						</td>
						<td style="border-bottom: 1px solid #ddd; border-left: 1px solid #ddd;">
							<%= results.get(test.testID).result %>
						</td>
					</tr>
			<% } %>
		</table>
	</body>
</html>

