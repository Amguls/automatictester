<%@ page import="java.util.List" %>
<%@ page import="server.PrivilegeChecker" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/deleteProblem.js"></script>
<h1>Problems</h1>
<table style="width:600px">
	<% 
	List<String> names = (List<String>)request.getAttribute("namesList");
	for(String name : names){ %>
		<tr id="problem_<%= name %>">
			<td>
			<a href="/AutomaticTester/problems?problem=
		<%= name %>"><%= name %></a>
			</td>
		    <% if (PrivilegeChecker.check(request, 1)) { %>
			<td>
				<button onclick="sendProblemToDelete('/AutomaticTester/deleteProblem', '<%= name %>')">Delete</button>
			</td>
			<% } %>
		</tr>
	<% } %>
</table>