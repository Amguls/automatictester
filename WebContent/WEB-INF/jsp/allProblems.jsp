<%@ page import="server.PrivilegeChecker" %>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Problems</title>
	</head>
	<body>
		<div id="problemList">
			<%@ include file = "problemList.jsp" %>
		</div>
		<% if (PrivilegeChecker.check(request, 1)) { %>
		<%@ include file = "createProblem.jsp" %>
		<% } %>
	</body>
</html>

