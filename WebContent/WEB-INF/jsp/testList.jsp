<%@ page import="java.util.List" %>
<%@ page import="server.PrivilegeChecker" %>
<% String testProblemName = request.getParameter("problem"); %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/deleteTest.js"></script>
<h1>Tests</h1>
<table style="width:600px">
<% List<String> names = (List<String>)request.getAttribute("testList");
	for(String name : names){ %>
		<tr id="test_<%= name %>">
		<td>
			<b>
			<% if (name.equals("[HIDDEN]")) { %>
				<%= name %>
			<% } else { %>
			<a href="/AutomaticTester/problems?problem=
			<%= request.getParameter("problem") %>
			&test=<%= name %>">
				<%= name %>
			</a>
			<% } %>
			</b>
		</td>
		<% if (PrivilegeChecker.check(request, 1)) { %>
		<td>
			<button onclick="sendTestToDelete('/AutomaticTester/deleteTest', '<%= testProblemName %>', '<%= name %>')">Delete</button>
		</td>
		<% } %>
		</tr>
<% } %>
</table>