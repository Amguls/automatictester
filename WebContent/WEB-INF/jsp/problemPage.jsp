<%@ page import="java.util.List" %>
<%@ page import="server.PrivilegeChecker" %>
<% String problemName = request.getParameter("problem"); %>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Problems - <%= problemName %></title>
	</head>
	<body>
		<%@ include file = "problemStatement.jsp" %>
		<% if (PrivilegeChecker.check(request, 1)) { %>
		<form method="POST" action="/AutomaticTester/deleteProblem?problem=<%= problemName%>">
			<input type="submit" value="Delete problem"/>
		</form>
		<% } %>
		<%@ include file = "runTests.jsp" %>
		<div id="testList">
			<%@ include file = "testList.jsp" %>
		</div>
		<% if (PrivilegeChecker.check(request, 1)) { %>
			<%@ include file = "createTest.jsp" %>
		<% } %>
	</body>
</html>

