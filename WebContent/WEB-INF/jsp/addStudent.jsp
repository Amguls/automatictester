<%@ page import="server.PrivilegeChecker" %>
<h1>Add New Student</h1>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/createStudent.js"></script>
<table>
	<tr>
		<td>Student Name:</td>
		<td><input type="text" id="studentName"/></td>
	</tr>
	<tr>
		<td>Student Username:</td>
		<td><input type="text" id="studentUsername"/></td>
	</tr>
	<tr>
		<td>Student Password:</td>
		<td><input type="text" id="password"/></td>
	</tr>
	<tr>
		<td>Is Privileged User:</td>
		<% if (PrivilegeChecker.check(request, 2)) { %>
			<td><input type="checkbox" id="privilege"/></td>
		<% } else { %>
			<td><input type="checkbox" id="privilege" style="display:none"/><i>Only admin may create privileged accounts</i></td>
		<% } %>
	</tr>
	<tr>
		<td>Student Email:</td>
		<td><input type="email" id="email"/></td>
	</tr>
	<tr>
		<td></td>
		<td><button onclick="sendStudentCreate('/AutomaticTester/createStudent')">Submit</button></td>
	</tr>
</table>

