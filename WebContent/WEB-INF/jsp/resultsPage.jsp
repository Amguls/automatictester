<%@ page import="java.util.List" %>
<%@ page import="restapi.TestData" %>
<!DOCTYPE html>

<html>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/retrieveTestInfo.js"></script>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Results - <%= request.getParameter("problem") %></title>
	</head>
	<body onload="retrieveTestInfo('/AutomaticTester/results.xml?problem=<%= request.getParameter("problem") %>')">
		<%@ include file = "problemStatement.jsp" %>
		<h1>Tests</h1>
		<table style="width:600px">
			<% List<TestData> tests = (List<TestData>)request.getAttribute("tests");
				for(TestData test : tests) { %>
					<tr>
						<td>
						<b>
						<% if (test.name.equals("[HIDDEN]")) { %>
							<%= test.name %>
						<% } else { %>
						<a href="/AutomaticTester/problems?problem=
							<%= request.getParameter("problem") %>
							&test=<%= test.name %>">
							<%= test.name %>
						</a>
						<% } %>
						</b>
						</td>
						<td id=<%= "result" + test.testID%>>
							Waiting...
						</td>
					</tr>
					<tr>
						<td><pre id=<%= "message" + test.testID%>>
							
						</pre></td>
					</tr>
			<% } %>
		</table>
	</body>
</html>

