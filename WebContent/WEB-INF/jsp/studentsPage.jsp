<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="restapi.StudentData" %>
<%@ page import="tester.Test" %>
<!DOCTYPE html>

<html>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Students</title>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/retrieveAllResults.js"></script>
		<script>setInterval(() => {retrieveAllResults("/AutomaticTester/allResults");}, 5000);</script>
	</head>
	<body>
		<div id="allResults">
			<%@ include file = "allResults.jsp" %>
		</div>
		<%@ include file = "addStudent.jsp" %>
		<%@ include file = "modifyStudent.jsp" %>
		<%@ include file = "bulkAdd.jsp" %>
	</body>
</html>

