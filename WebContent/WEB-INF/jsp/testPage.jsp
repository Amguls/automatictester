<%@ page import="server.PrivilegeChecker" %>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file = "common.jsp" %>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/deleteTest.js"></script>
		<title>Test - <%= request.getParameter("test") %></title>
	</head>
	<body>
		<%@ include file = "problemStatement.jsp" %>
		<h1>Test <%= request.getParameter("test") %></h1>
		<% if ((Boolean)request.getAttribute("isHidden")) { %>
		<i style="color:blue">Test is hidden from students</i>
		<% } %>
		<h3>Test Input</h3>
		<p><pre><%= request.getAttribute("testInput") %></pre></p>
		<h3>Test Output</h3>
		<p><pre><%= request.getAttribute("testOutput") %></pre></p>
		<% if (PrivilegeChecker.check(request, 1)) { %>
		<form accept-charset="utf-8" method="POST" action="/AutomaticTester/deleteTest?problem=
				<%=  request.getParameter("problem") %>
				&test=<%=  request.getParameter("test") %>">
			<input type="submit" value="Delete test"/>
		</form>
		<% } %>
	</body>
</html>