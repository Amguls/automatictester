<h1>Create New Problem</h1>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/createProblem.js"></script>
<table>
	<tr>
		<td>
			<table>
				<tr>
					<td>Problem Name:</td>
					<td><input type="text" id="problemName"/></td>
				</tr>
				<tr>
					<td>Problem Statement (HTML)</td>
					<td>
						<!-- TODO: helpful buttons to help with html -->
						<textarea style="resize:none;" cols="80" rows="20" id="problemStatement" oninput="document.getElementById('preview').innerHTML=this.value;"></textarea>
					</td>
				</tr>
				<tr>
					<td>Solution Type:</td>
					<td>
						<select id="solutionType">
							<option value="python">Python</option>
							<option value="java">Java</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Main class name (Java only)</td>
					<td><input type="text" id="fileName"/></td>
				</tr>
				<tr>
					<td>Solution Code:</td>
					<td><textarea cols="80" rows="20" id="solution" style="resize:none;"></textarea></td>
				</tr>
				<tr>
					<td>Checker Python:</td>
					<td><textarea cols="80" rows="20"  id="checker" style="resize:none;"></textarea></td>
				</tr>
				<tr>
					<td/>
					<td><button onclick="sendProblemCreate('/AutomaticTester/createProblem')">Submit</button></td>
				</tr>
			</table>
		</td>
		<td valign="top">
				<pre style="white-space: pre-wrap; min-height:400px; width:600px; margin:0; border:1px solid black;" id="preview"></pre>
				<script>document.getElementById('preview').innerHTML=document.getElementById('problemStatement').value</script>
		</td>
	</tr>
</table>