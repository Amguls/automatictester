<%@ page import="server.PrivilegeChecker" %>
<!DOCTYPE html>
<html>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Automatic Tester</title>
	</head>
	<body>
		<% String name = (String)request.getAttribute("loggedName"); %>
		<% if (name == null) { %>
		<p>Not logged in</p><a href="/AutomaticTester/login">Log in</a>
		<% } else { %>
		<p>Logged in as <%= name %></p><a href="/AutomaticTester/logout">Log out</a>
		<% } %>
		<h1>Select option</h1>
		<a href="/AutomaticTester/problems">View problems</a><br/>
		<% if (PrivilegeChecker.check(request,1)) { %>
			<a href="/AutomaticTester/students">View students and results</a>
		<%
			}
		%>
	</body>
</html>

