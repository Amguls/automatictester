<h1>Create New Test</h1>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/createTest.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sendFormInteractive.js"></script>
<table>
	<tr>
		<td>Test Name:</td>
		<td><input type="text" id="testName"/></td>
		<td>Output:</td>
	</tr>
	<tr>
		<td>Input For Test:</td>
		<td><textarea rows="10" cols="40" id="newTestInput" style="resize:none;"></textarea></td>
		<td><textarea rows="10" cols="40" id="newTestOutput" style="resize:none;" readonly></textarea></td>
	</tr>
	<tr>
		<td>Is hidden test:</td>
		<td><input type="checkbox" id="testHidden" value="Hidden"/></td>
	</tr>
	<tr>
		<td/>
		<td>
			<button onclick="sendTestCreate('/AutomaticTester/createTest','<%= request.getParameter("problem") %>')" value="Submit">Submit</button>
			<button onclick="sendNewTestInput('/AutomaticTester/run','<%= request.getParameter("problem") %>')" value="Execute">View Output</button>	
		</td>
	</tr>
</table>