<%@ page import="server.PrivilegeChecker" %>
<h1>Submit Solution</h1>
<style type="text/css" media="screen">
#editorParent {
    width: 525px;
    height: 200px;
    display:inline-block;
    position:relative;
}
#program {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
}
</style>
<% if (!PrivilegeChecker.isLoggedIn(request)) { %>
<p>Please <a href="/AutomaticTester/login">log in</a> to be able to submit</p>
<% }  else { %>
<form id="solutionForm" accept-charset="utf-8" method="POST" enctype="multipart/form-data" action="/AutomaticTester/results?problem=<%= request.getParameter("problem") %>">
	<table>
		<tr>
			<td>Solution type:</td>
			<td>
				<select name="solutionType" id="solutionType">
					<option value="python">Python</option>
					<option value="java">Java</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Main class name (Java only):</td>
			<td><input type="text" name="fileName" id="fileName"/></td>
		</tr>
	</table>
	<textarea name="source" style="display:none" id="hiddenTextarea">Put program here...</textarea>
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.6/ace.js" type="text/javascript" charset="utf-8"></script>

<table>
	<tr>
		<td><textarea rows="13" cols="20" name="input" id="input" style="resize:none">Put test input here...</textarea></td>
		<td>
			<div id="editorParent">
				<div id="program">Put program here...</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea rows="10" cols="93" name="output" id="output" style="resize:none" readonly>Output will appear here.</textarea>
		</td>
	</tr>
</table>

<script>
	var editor = ace.edit("program");
	editor.session.setMode("ace/mode/python");
	
	var language = document.getElementById("solutionType");
	language.addEventListener("change", function() {
		editor.getSession().setMode("ace/mode/"+language.value);
	});
	
	var textarea = document.getElementById("hiddenTextarea");
	editor.getSession().on("change", function () {
	    textarea.value = editor.getSession().getValue();
	});
	
	// Use saved value from textarea if it exists.
	editor.setValue(textarea.value, -1);
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sendFormInteractive.js"></script>
<button onclick="sendProgramAndInput('/AutomaticTester/run')">Run program</button>
<input type="submit" value="Submit for testing" form="solutionForm">
<% } %>