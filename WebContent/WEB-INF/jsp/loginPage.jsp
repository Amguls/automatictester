<!DOCTYPE html>
<html>
	<head>
		<%@ include file = "common.jsp" %>
		<title>Automatic Tester</title>
	</head>
	<body>
		<h1>Log in</h1>
		<form method="POST">
			<table>
				<tr>
					<td>Username</td>
					<td><input type="text" name="username"></input></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="password"></input></td>
				</tr>
				<tr>
					<td/>
					<td><input type="submit" value="Submit"></input></td>
				</tr>
			</table>
		</form>
		<a href="/AutomaticTester/requestReset">Forgot Password</a>
	</body>
</html>

