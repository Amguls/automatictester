<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="restapi.StudentData" %>
<%@ page import="tester.Test" %>
<h1>Students</h1>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/deleteStudent.js"></script>
<table>
	<tr>
		<th/>
		<th>
			Student
		</th>
		<% List<String> problems = (List<String>)request.getAttribute("problems"); 
		  for (String problem : problems) { %>
		<th>
			<%= problem %>
		</th>
		<% } %>
	</tr>
	<% Map<StudentData, Map<String, Test.Result> > studentUsers = (Map<StudentData, Map<String, Test.Result> >)request.getAttribute("results");
		for (StudentData data : studentUsers.keySet()) { %>
			<tr id="student_<%= data.userName %>">
				<td><button onclick="sendStudentToDelete('/AutomaticTester/deleteStudent', '<%= data.userName %>')">Delete</button>
				<td>
					<b><%= data.realName %></b>
				</td>
				<% for (String problem : problems) {
					boolean haveSubmission = true;
					Test.Result r = studentUsers.get(data).get(problem);
				    String message, color;
				    if (r == Test.Result.PASSED) {
				   	   message = "Passed all tests";
				   	   color = "green";
			       } else if (r == Test.Result.INVALID) {
			    	   haveSubmission = false;
			    	   message = "Not submitted";
				   	   color = "white";
			       } else if (r == Test.Result.PROCESSING) {
			    	   haveSubmission = false;
			    	   message = "Processing...";
			    	   color = "yellow";
			       } else {
			    	   message = "Failed some tests";
				   	   color = "red";
			       }
				%>
				<td bgcolor="<%= color %>">
				<% if (haveSubmission) { %>
					<a href="/AutomaticTester/submission?problem=<%= problem %>&student=<%= data.userName %>"><%= message %></a>
				<% } else { %>
				    <%= message %>
				<% } %>
				</td>
				<% } %>
			</tr>
	<% } %>
</table>