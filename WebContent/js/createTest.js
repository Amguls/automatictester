function createTest(url, data) {
	post(url, data, (status, response) => {
    	if (status == 200) {
    		// This means success.
    		document.getElementById("testList").innerHTML = response;
		}
	});
}

function sendTestCreate(url, problem) {
	console.log("Hello");
	if (document.getElementById("testName").value == "") {
		return;
	}
	var data = {
		'problem'    : problem,
		'testName'   : document.getElementById("testName").value,
		'testHidden' : document.getElementById("testHidden").checked,
		'testInput'  : document.getElementById("newTestInput").value
	};
	document.getElementById("testName").value = "";
	document.getElementById("testHidden").checked = false,
	document.getElementById("newTestInput").value = "";
	createTest(url, data);
}