function createProblem(url, data) {
	post(url, data, (status, response) => {
    	if (status == 200) {
    		// This means success.
    		document.getElementById("problemList").innerHTML = response;
		}
	});
}

function sendProblemCreate(url) {
	if (document.getElementById("problemName").value == "") {
		return;
	}
	if (document.getElementById("problemStatement").value == "") {
		return;
	}
	if (document.getElementById("solutionType").value == "java" && document.getElementById("fileName").value == "") {
		return;
	}
	var data = {
		'problemName'      : document.getElementById("problemName").value,
		'problemStatement' : document.getElementById("problemStatement").value,
		'solution'         : document.getElementById("solution").value,
		'solutionType'     : document.getElementById("solutionType").value,
		'fileName'         : document.getElementById("fileName").value,
		'checker'          : document.getElementById("checker").value
	};
	document.getElementById("problemName").value = "";
	document.getElementById("problemStatement").value = "";
	document.getElementById("solution").value = "";
	document.getElementById("fileName").value = "";
	document.getElementById("checker").value = "";
	document.getElementById("preview").innerHTML = "";
	createProblem(url, data);
}