function retrieveTestInfo(url) {
	get(url, (status, responseXML) => {
    	if (status == 200) {
    		// This means success.
    		var repeat = false;
    		var xmlDoc = new DOMParser().parseFromString(responseXML,"text/xml")
    		var tests = xmlDoc.getElementsByTagName("testStatus");
    		for (var i = 0; i < tests.length; i++) {
    			var id = tests[i].getElementsByTagName("testID")[0].childNodes[0].nodeValue;
    			var resultFinal = tests[i].getElementsByTagName("result")[0].childNodes[0].nodeValue;
    			var resultStatus = tests[i].getElementsByTagName("resultString")[0].childNodes[0].nodeValue;
    			var resultMessage = tests[i].getElementsByTagName("message")[0].childNodes[0].nodeValue;
    			
    			document.getElementById("result"+id).innerText = resultStatus;
    			document.getElementById("message"+id).innerText = resultMessage;
    			
    			if (resultFinal == 0) {
    				repeat = true;
    			}
    		}
    		// Not got results for all tests, requery.
    		if (repeat) {
    			setTimeout(() => retrieveTestInfo(url), 1000);
    		}
		} else {
			// Went wrong, try again.
			setTimeout(() => retrieveTestInfo(url), 1000);
		}
	});
}