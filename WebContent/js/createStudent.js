function createStudent(url, data) {
	post(url, data, (status, response) => {
    	if (status == 200) {
    		// This means success.
    		document.getElementById("allResults").innerHTML = response;
		}
	});
}

function sendStudentCreate(url) {
	if (document.getElementById("studentName").value == "") {
		return;
	}
	if (document.getElementById("studentUsername").value == "") {
		return;
	}
	if (document.getElementById("password").value == "") {
		return;
	}
	if (document.getElementById("email").value == "") {
		return;
	}
	var data = {
		'studentName'     : document.getElementById("studentName").value,
		'studentUsername' : document.getElementById("studentUsername").value,
		'isPrivileged'    : document.getElementById("privilege").checked,
		'password'        : document.getElementById("password").value,
		'email'           : document.getElementById("email").value
	};
	document.getElementById("studentName").value = "";
	document.getElementById("studentUsername").value = "";
	document.getElementById("password").value = "";
	document.getElementById("privilege").checked = false;
	document.getElementById("email").value = "";
	createStudent(url, data);
}

function bulkAddStudent(url) {
	var csv = document.getElementById("csv").value;
	var lines = csv.split("\n");
	for (var line in lines) {
		var parts = lines[line].split(",");
		if (parts.length != 3)
			continue;
		var data = {
			'studentName'     : parts[0].trim(),
			'studentUsername' : parts[1].trim(),
			'password'        : parts[2].trim(),
			'email'        : parts[3].trim()
		};
		createStudent(url, data);
	}
	document.getElementById("csv").value = "";
}