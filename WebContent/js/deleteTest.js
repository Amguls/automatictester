function deleteTest(url, data) {
	post(url, data, (status, _) => {
    	if (status == 200) {
    		// This means success.
    		var row = document.getElementById("test_" + data['test']);
    		row.parentNode.removeChild(row);
		}
	});
}

function sendTestToDelete(url, problem, name) {
	var data = {
		'problem' : problem,
		'test'    : name
	};
	deleteTest(url, data);
}