var isIE;

function get(url, callback) {
	var req = initRequest();
    req.open("GET", url, true);
    req.onload = () => {
		callback(req.status, req.responseText);
    }
    req.send();
}

function post(url, data, callback) {
	var req = initRequest();
    req.open("POST", url, true);
    req.onload = () => {
		callback(req.status, req.responseText);
    }
    var formData = new FormData();
    for (var name in data) {
        formData.append(name, data[name]);
    }
    req.send(formData);
}

function initRequest() {
    if (window.XMLHttpRequest) {
        if (navigator.userAgent.indexOf('MSIE') != -1) {
            isIE = true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}