function modifyStudent(url, data) {
	post(url, data, (status, response) => {
    	if (status == 200) {
    		// This means success.
    		document.getElementById("allResults").innerHTML = response;
		}
	});
}

function sendStudentModify(url) {
	if (document.getElementById("modifyStudentUsername").value == "") {
		return;
	}
	if (document.getElementById("modifyPassword").value == "") {
		return;
	}
	var data = {
		'studentUsername' : document.getElementById("modifyStudentUsername").value,
		'password'        : document.getElementById("modifyPassword").value
	};
	document.getElementById("modifyStudentUsername").value = "";
	document.getElementById("modifyPassword").value = "";
	modifyStudent(url, data);
}