function deleteProblem(url, data) {
	post(url, data, (status, _) => {
    	if (status == 200) {
    		// This means success.
    		var row = document.getElementById("problem_" + data['problem']);
    		row.parentNode.removeChild(row);
		}
	});
}

function sendProblemToDelete(url, problem) {
	var data = {
		'problem' : problem,
	};
	deleteProblem(url, data);
}