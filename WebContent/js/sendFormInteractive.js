function sendTestExecute(url, data, outputID) {
	post(url, data, (status, responseXML) => {
    	if (status == 200) {
    		// This means success.
    		var xmlDoc = new DOMParser().parseFromString(responseXML,"text/xml")
    		var tests = xmlDoc.getElementsByTagName("executionResult");
    		var status = tests[0].getElementsByTagName("status")[0].childNodes[0].nodeValue;
    		var executionOutput;
    		if (tests[0].getElementsByTagName("output")[0].childNodes.length > 0) {
    			executionOutput = tests[0].getElementsByTagName("output")[0].childNodes[0].nodeValue;
    		} else {
    			executionOutput = "";
    		}
    		var message;
    		if (tests[0].getElementsByTagName("message")[0].childNodes.length > 0) {
    			message = tests[0].getElementsByTagName("message")[0].childNodes[0].nodeValue;
    		} else {
    			message = "";
    		}
    		
    		if (status != 0) {
    			document.getElementById(outputID).value = executionOutput + message;
    		} else {
    			document.getElementById(outputID).value = executionOutput;
    		}
		}
	});
}

function sendProgramAndInput(url) {
	var data = {
		'source'   : editor.getValue(),
		'input'    : document.getElementById("input").value,
		'type'     : document.getElementById("solutionType").value,
		'fileName' :  document.getElementById("fileName").value
	};
	document.getElementById("output").value = "Waiting...";
	sendTestExecute(url, data, "output");
}

function sendNewTestInput(url, problemName) {
	var data = {
		'problem' : problemName,
		'input'   : document.getElementById("newTestInput").value
	};
	document.getElementById("newTestOutput").value = "Waiting...";
	sendTestExecute(url, data, "newTestOutput");
}