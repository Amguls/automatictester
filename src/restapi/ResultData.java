package restapi;

import tester.Test;

public class ResultData {
    public Test.Result result;
    public String message;
    public ResultData(Test.Result result, String message) {
        this.result = result; this.message = message;
    }
    public ResultData(int result, String message) {
        this.result = Test.Result.values()[result]; this.message = message;
    }
}
