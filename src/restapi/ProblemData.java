package restapi;

import java.io.InputStream;

public class ProblemData implements Comparable<ProblemData> {
    public String name;
    public InputStream statement;
    public ProblemData(String name, InputStream statement) {
        this.name = name; this.statement = statement;
    }
	@Override
	public int compareTo(ProblemData other) {
		return name.compareTo(other.name);
	}
}
