package restapi;

public class TestConfig {
    public boolean ignoreCase = false;
    public boolean isHidden = true;
    public TestConfig(boolean isHidden, boolean ignoreCase) {
        this.isHidden = isHidden; this.ignoreCase = ignoreCase;
    }
    public TestConfig(boolean isHidden) {
        this.isHidden = isHidden;
    }
    public TestConfig() {}
}
