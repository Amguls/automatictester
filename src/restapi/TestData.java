package restapi;

import java.io.InputStream;

public class TestData {
	public int testID;
    public String name;
    public InputStream input;
    public InputStream output;
    public TestData(int testID, String name, InputStream input, InputStream output) {
        this.testID = testID; this.name = name; this.input = input; this.output = output;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TestData))
            return false;

        return (obj == this);
    }
}