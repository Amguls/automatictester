package restapi;

import java.io.*;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.*;
import java.util.*;

import tester.Test;

// A class providing an API for the database - this is tightly coupled with the underlying database design.
// Contains a large number of functions, but most are just allowing different keys for the database queries.
public class DatabaseConnection {

    private Connection connection;

    private final String DB_URL = "jdbc:mysql://localhost:3306/automatic_tester";
    private final String USER = "rbl24";
    private final String PASS = "";

    private static DatabaseConnection instance = new DatabaseConnection();
    public static DatabaseConnection getInstance() {
        return instance;
    }
    
    private String genSalt() {
    	byte[] bytes = new byte[32];
    	SecureRandom random = new SecureRandom();
    	random.nextBytes(bytes);
    	return new String(bytes);
    }
    
    private String getSalt(int studentID) {
    	String result = null;
        String sqlSelect = "SELECT salt FROM students WHERE studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = results.getString("salt");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    private String getSalt(String studentUsername) {
    	int studentID = getStudentID(studentUsername);
    	return getSalt(studentID);
    }
    
    private byte[] hashPassword(String password, String salt) {
    	if (salt == null || password == null) {
    		return null;
    	}
    	
    	MessageDigest digest;
    	try {
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
			return null;
		}
    	
    	String pepper = "GLjBRmW0RmCwnbOe15fO";
    	
    	return digest.digest((password + salt + pepper).getBytes());
    }

    private DatabaseConnection() {
    	try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            System.err.println("Failed to connect to database");
            e.printStackTrace();
        }
    }

    public void addStudent(String userName, String realName, String password, String email, boolean isPrivileged) {
    	String salt = genSalt();
    	byte[] hashedPass = hashPassword(password, salt);
    	
    	if (hashedPass == null) {
    		return;
    	}
    	
        String sqlInsert = "INSERT INTO students(studentUsername, studentName, password, privilegeLevel, salt, email) VALUES(?,?,?,?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setString(1, userName);
            statement.setString(2, realName);
            statement.setBytes(3, hashedPass);
            statement.setInt(4, (isPrivileged ? 1 : 0));
            statement.setString(5, salt);
            statement.setString(6, email);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public String getStudentEmail(int studentID) {
    	String email = null;
    	String sqlSelect = "SELECT email FROM students WHERE studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                email = results.getString("email");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return email;
    }
    public String getStudentEmail(String studentName) {
    	int studentID = getStudentID(studentName);
    	return getStudentEmail(studentID);
    }

    public void updateStudent(int studentID, String password) {
    	byte[] hashedPass = hashPassword(password, getSalt(studentID));
    	
    	if (hashedPass == null) {
    		return;
    	}
    	
        String sqlUpdate = "UPDATE students SET password=? WHERE studentID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlUpdate)) {
            statement.setBytes(1, hashedPass);
            statement.setInt(2, studentID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateStudent(String userName, String password) {
    	int studentID = getStudentID(userName);
    	updateStudent(studentID, password);
    }
    
    public StudentData getStudent(int studentID) {
    	StudentData result = null;
        String sqlSelect = "SELECT studentName, studentUsername FROM students WHERE studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                String name = results.getString("studentName");
                String userName = results.getString("studentUsername");
                result = new StudentData(studentID, name, userName, 0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int getStudentID(String userName) {
        int result = -1;
        String sqlSelect = "SELECT studentID FROM students WHERE studentUsername=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setString(1, userName);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = results.getInt("studentID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public void deleteStudent(int studentID) {
    	deleteResultsForStudent(studentID);
    	deleteSubmissionsForStudent(studentID);
    	deleteResetRequestForStudent(studentID);
    	
        String sqlDelete =
                "DELETE FROM students WHERE students.studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, studentID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteStudent(String userName) {
        int studentID = getStudentID(userName);
        deleteStudent(studentID);
    }
    
    public void deleteResultsForStudent(int studentID) {
        String sqlDelete =
                "DELETE results FROM students" +
        " JOIN results ON students.studentID=results.studentID WHERE students.studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, studentID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getStudentRealName(int studentID) {
        String result = null;
        String sqlSelect = "SELECT studentName FROM students WHERE studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = results.getString("studentName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public String getStudentRealName(String studentUsername) {
        int studentID = getStudentID(studentUsername);
        return getStudentRealName(studentID);
    }

    public boolean haveResult(int studentID, int testID) {
        boolean result = false;

        String sqlSelect = "SELECT * FROM results WHERE studentID=? AND testID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);
            statement.setInt(2, testID);

            ResultSet results = statement.executeQuery();

            result = results.next();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public boolean haveResult(int studentID, String problemName, String testName) {
        int testID = getTestID(problemName, testName);
        return haveResult(studentID, testID);
    }
    public boolean haveResult(String studentName, int testID) {
        int studentID = getStudentID(studentName);
        return haveResult(studentID, testID);
    }
    public boolean haveResult(String studentName, String problemName, String testName) {
        int studentID = getStudentID(studentName);
        int testID = getTestID(problemName, testName);
        return haveResult(studentID, testID);
    }

    public void updateResult(int studentID, int testID, int resultValue, String message) {
        String sqlUpdate = "UPDATE results SET resultValue=?, resultMessage=? WHERE studentID=? AND testID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlUpdate)) {
            statement.setInt(1, resultValue);
            statement.setString(2, message);
            statement.setInt(3, studentID);
            statement.setInt(4, testID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateResult(String studentName, int testID, int resultValue, String message) {
        int studentID = getStudentID(studentName);
        updateResult(studentID, testID, resultValue, message);
    }
    public void updateResult(String studentName, int problemID, String testName, int resultValue, String message) {
        int studentID = getStudentID(studentName);
        int testID = getTestID(problemID, testName);
        updateResult(studentID, testID, resultValue, message);
    }
    public void updateResult(int studentID, int problemID, String testName, int resultValue, String message) {
        int testID = getTestID(problemID, testName);
        updateResult(studentID, testID, resultValue, message);
    }
    public void updateResult(int studentID, String problemName, String testName, int resultValue, String message) {
        int testID = getTestID(problemName, testName);
        updateResult(studentID, testID, resultValue, message);
    }
    public void updateResult(String studentName, String problemName, String testName, int resultValue, String message) {
        int studentID = getStudentID(studentName);
        int testID = getTestID(problemName, testName);
        updateResult(studentID, testID, resultValue, message);
    }

    public void addResult(int studentID, int testID, int resultValue, String message) {
        if (haveResult(studentID, testID)) {
            updateResult(studentID, testID, resultValue, message);
            return;
        }

        String sqlInsert = "INSERT INTO results(studentID, testID, resultValue, resultMessage) VALUES(?,?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setInt(1, studentID);
            statement.setInt(2, testID);
            statement.setInt(3, resultValue);
            statement.setString(4, message);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addResult(int studentID, int problemID, String testName, int resultValue, String message) {
        int testID = getTestID(problemID, testName);
        addResult(studentID, testID, resultValue, message);
    }
    public void addResult(int studentID, String problemName, String testName, int resultValue, String message) {
        int testID = getTestID(problemName, testName);
        addResult(studentID, testID, resultValue, message);
    }
    public void addResult(String studentName, int testID, int resultValue, String message) {
        int studentID = getStudentID(studentName);
        addResult(studentID, testID, resultValue, message);
    }
    public void addResult(String studentName, int problemID, String testName, int resultValue, String message) {
        int studentID = getStudentID(studentName);
        int testID = getTestID(problemID, testName);
        addResult(studentID, testID, resultValue, message);
    }
    public void addResult(String studentName, String problemName, String testName, int resultValue, String message) {
        int studentID = getStudentID(studentName);
        int problemID = getProblemID(problemName);
        int testID = getTestID(problemID, testName);
        addResult(studentID, testID, resultValue, message);
    }
    public void addResult(String studentName, String problemName, String testName, ResultData data) {
        addResult(studentName, problemName, testName, data.result.ordinal(), data.message);
    }
    public void addResult(int studentID, String problemName, String testName, ResultData data) {
        addResult(studentID, problemName, testName, data.result.ordinal(), data.message);
    }
    public void addResult(int studentID, int testID, ResultData data) {
        addResult(studentID, testID, data.result.ordinal(), data.message);
    }
    public void addResult(String studentName, int problemID, String testName, ResultData data) {
        addResult(studentName, problemID, testName, data.result.ordinal(), data.message);
    }
    public void addResult(String studentName, int testID, ResultData data) {
        addResult(studentName, testID, data.result.ordinal(), data.message);
    }

    public ResultData getResultData(int studentID, int testID) {
        ResultData result = null;

        String sqlSelect =
                "SELECT resultValue, resultMessage FROM results WHERE studentID=? AND testID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);
            statement.setInt(2, testID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = new ResultData(results.getInt("resultValue"), results.getString("resultMessage"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public ResultData getResultData(String studentName, int testID) {
        int studentID = getStudentID(studentName);
        return getResultData(studentID, testID);
    }
    public ResultData getResultData(String studentName, int problemID, String testName) {
        int studentID = getStudentID(studentName);
        int testID = getTestID(problemID, testName);
        return getResultData(studentID, testID);
    }
    public ResultData getResultData(String studentName, String problemName, String testName) {
        int studentID = getStudentID(studentName);
        int testID = getTestID(problemName, testName);
        return getResultData(studentID, testID);
    }
    public ResultData getResultData(int studentID, int problemID, String testName) {
        int testID = getTestID(problemID, testName);
        return getResultData(studentID, testID);
    }
    public ResultData getResultData(int studentID, String problemName, String testName) {
        int testID = getTestID(problemName, testName);
        return getResultData(studentID, testID);
    }

    // Returns a mapping from test IDs to result values.
    public Map<Integer, ResultData> getTestResults(int studentID, int problemID) {
        Map<Integer, ResultData> passed = new HashMap<>();

        String sqlSelect =
                "SELECT results.testID, results.resultValue, results.resultMessage FROM results JOIN tests ON results.testID=tests.testID" +
                        " WHERE results.studentID=? AND tests.problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);
            statement.setInt(2, problemID);

            ResultSet results = statement.executeQuery();

            while (results.next()) {
                int testID = results.getInt("testID");
                ResultData result = new ResultData(results.getInt("resultValue"), results.getString("resultMessage"));
                passed.put(testID, result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return passed;
    }
    public Map<Integer, ResultData> getTestResults(String studentName, int problemID) {
        int studentID = getStudentID(studentName);
        return getTestResults(studentID, problemID);
    }
    public Map<Integer, ResultData> getTestResults(int studentID, String problemName) {
        int problemID = getProblemID(problemName);
        return getTestResults(studentID, problemID);
    }
    public Map<Integer, ResultData> getTestResults(String studentName, String problemName) {
        int studentID = getStudentID(studentName);
        int problemID = getProblemID(problemName);
        return getTestResults(studentID, problemID);
    }

    public List<TestData> getPublicTests(int problemID) {
        List<TestData> tests = new ArrayList<>();

        String sqlSelect =
                "SELECT tests.testID, testName, testInput, testOutput" +
                        " FROM tests LEFT JOIN test_config ON tests.testID=test_config.testID" +
                        " WHERE problemID=? AND (testIsHidden IS NULL OR NOT testIsHidden);";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);

            ResultSet results = statement.executeQuery();

            while (results.next()) {
            	int id = results.getInt("testID");
                String name = results.getString("testName");
                Blob testInput = results.getBlob("testInput");
                Blob testOutput = results.getBlob("testOutput");
                tests.add(new TestData(id, name, testInput.getBinaryStream(), testOutput.getBinaryStream()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tests;
    }
    public List<TestData> getPublicTests(String problemName) {
        int problemID = getProblemID(problemName);
        return getPublicTests(problemID);
    }


    public List<TestData> getAllTests(int problemID) {
        List<TestData> tests = new ArrayList<>();

        String sqlSelect =
                "SELECT testID, testName, testInput, testOutput FROM tests WHERE problemID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);

            ResultSet results = statement.executeQuery();

            while (results.next()) {
            	int id = results.getInt("testID");
                String name = results.getString("testName");
                Blob testInput = results.getBlob("testInput");
                Blob testOutput = results.getBlob("testOutput");
                tests.add(new TestData(id, name, testInput.getBinaryStream(), testOutput.getBinaryStream()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tests;
    }
    public List<TestData> getAllTests(String problemName) {
        int problemID = getProblemID(problemName);
        return getAllTests(problemID);
    }

    public int getProblemID(String name) {
        int result = -1;
        String sqlSelect = "SELECT problemID FROM problems WHERE problemName=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setString(1, name);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = results.getInt("problemID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<ProblemData> getAllProblems() {
        List<ProblemData> result = new ArrayList<>();
        String sqlSelect = "SELECT problemName, problemStatement FROM problems";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            ResultSet results = statement.executeQuery();

            while (results.next()) {
                String problemName = results.getString("problemName");
                InputStream problemStatement = results.getBlob("problemStatement").getBinaryStream();
                result.add(new ProblemData(problemName, problemStatement));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        result.sort(new Comparator<ProblemData>() {
			@Override
			public int compare(ProblemData arg0, ProblemData arg1) {
				if (arg0.name.length() < arg1.name.length()) {
					return -1;
				}
				if (arg0.name.length() > arg1.name.length()) {
					return 1;
				}
				return arg0.compareTo(arg1);
			}
        });
        return result;
    }

    public ProblemData getProblemData(int problemID) {
        ProblemData result = null;
        String sqlSelect = "SELECT problemName, problemStatement FROM problems WHERE problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                String problemName = results.getString("problemName");
                InputStream problemStatement = results.getBlob("problemStatement").getBinaryStream();
                result = new ProblemData(problemName, problemStatement);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public ProblemData getProblemData(String problemName) {
        int problemID = getProblemID(problemName);
        return getProblemData(problemID);
    }
    
    public InputStream getProblemChecker(int problemID) {
        InputStream result = null;
        String sqlSelect = "SELECT problemChecker FROM problems WHERE problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                Blob blob = results.getBlob("problemChecker");
                if (!results.wasNull()) {
                	result = blob.getBinaryStream();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public InputStream getProblemChecker(String problemName) {
        int problemID = getProblemID(problemName);
        return getProblemChecker(problemID);
    }

    public InputStream getProblemSolution(int problemID) {
        InputStream result = null;
        String sqlSelect = "SELECT problemSolution FROM problems WHERE problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = results.getBlob("problemSolution").getBinaryStream();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public InputStream getProblemSolution(String problemName) {
        int problemID = getProblemID(problemName);
        return getProblemSolution(problemID);
    }

    public String getSolutionName(int problemID) {
        String result = null;
        String sqlSelect = "SELECT solutionName FROM problems WHERE problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                result = results.getString("solutionName");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public String getSolutionName(String problemName) {
        int problemID = getProblemID(problemName);
        return getSolutionName(problemID);
    }

    public int getTestID(int problemID, String name) {
        int result = -1;
        String sqlSelect = "SELECT testID FROM tests WHERE problemID=? AND testName=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, problemID);
            statement.setString(2, name);

            ResultSet results = statement.executeQuery();

            if (!results.next()) {
                return result;
            }
            assert results.isLast();
            result = results.getInt("testID");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public int getTestID(String problemName, String testName) {
        int problemID = getProblemID(problemName);
        return getTestID(problemID, testName);
    }

    public void addTest(int problemID, String name, InputStream testInput, InputStream testOutput) {
        String sqlInsert = "INSERT INTO tests(problemID, testName, testInput, testOutput) VALUES(?,?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setInt(1, problemID);
            statement.setString(2, name);
            statement.setBlob(3, testInput);
            statement.setBlob(4, testOutput);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addTest(int problemID, String name, InputStream testInput, InputStream testOutput, TestConfig config) {
        addTest(problemID, name, testInput, testOutput);
        addTestConfig(problemID, name, config);
    }
    public void addTest(String problemName, String name, InputStream testInput, InputStream testOutput) {
        int problemID = getProblemID(problemName);
        addTest(problemID, name, testInput, testOutput);
    }
    public void addTest(String problemName, String name, InputStream testInput, InputStream testOutput, TestConfig config) {
        int problemID = getProblemID(problemName);
        addTest(problemID, name, testInput, testOutput, config);
    }

    public TestData getTest(int testID) {
        TestData result = null;
        String sqlSelect = "SELECT testName, testInput, testOutput FROM tests WHERE testID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, testID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                String name = results.getString("testName");
                InputStream input = results.getBlob("testInput").getBinaryStream();
                InputStream output = results.getBlob("testOutput").getBinaryStream();
                result = new TestData(testID, name, input, output);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public TestData getTest(int problemID, String testName) {
        int testID = getTestID(problemID, testName);
        return getTest(testID);
    }
    public TestData getTest(String problemName, String testName) {
        int testID = getTestID(problemName, testName);
        return getTest(testID);
    }

    public void addTestConfig(int testID, TestConfig config) {
        String sqlInsert = "INSERT INTO test_config(testID, testIsHidden, testIgnoreCase) VALUES(?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setInt(1, testID);
            statement.setBoolean(2, config.isHidden);
            statement.setBoolean(3, config.ignoreCase);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addTestConfig(int problemID, String testName, TestConfig config) {
    	int testID = getTestID(problemID, testName);
    	addTestConfig(testID, config);
    }
    public void addTestConfig(String problemName, String testName, TestConfig config) {
    	int testID = getTestID(problemName, testName);
    	addTestConfig(testID, config);
    }

    public TestConfig getTestConfig(int testID) {
        TestConfig result = null;
        String sqlSelect = "SELECT testIsHidden, testIgnoreCase FROM test_config WHERE testID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, testID);

            ResultSet results = statement.executeQuery();

            if (results.next()) {
                assert results.isLast();
                boolean isHidden = results.getBoolean("testIsHidden");
                boolean ignoreCase = results.getBoolean("testIgnoreCase");
                result = new TestConfig(isHidden, ignoreCase);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    public TestConfig getTestConfig(int problemID, String testName) {
        int testID = getTestID(problemID, testName);
        return getTestConfig(testID);
    }
    public TestConfig getTestConfig(String problemName, String testName) {
        int testID = getTestID(problemName, testName);
        return getTestConfig(testID);
    }
    
    public void addProblem(String name, InputStream problemStatement, InputStream problemSolution, String solutionName, InputStream checker) {
        String sqlInsert = "INSERT INTO problems(problemName, problemStatement, problemSolution, solutionName, problemChecker) VALUES(?,?,?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setString(1, name);
            statement.setBlob(2, problemStatement);
            statement.setBlob(3, problemSolution);
            statement.setString(4, solutionName);
            statement.setBlob(5, checker);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addProblem(String name, byte[] problemStatement, byte[] problemSolution, String solutionName, byte[] checker) {
        addProblem(name, new ByteArrayInputStream(problemStatement), new ByteArrayInputStream(problemSolution), solutionName, new ByteArrayInputStream(checker));
    }
    public void addProblem(String name, File problemStatement, File problemSolution, File checker) {
        try {
            addProblem(name,
                    new FileInputStream(problemStatement),
                    new FileInputStream(problemSolution),
                    problemSolution.getName(),
                    new FileInputStream(checker));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void addProblem(String name, Path problemStatement, Path problemSolution, Path checker) {
        addProblem(name, problemStatement.toFile(), problemSolution.toFile(), checker.toFile());
    }

    public void addProblem(String name, InputStream problemStatement, InputStream problemSolution, String solutionName) {
        String sqlInsert = "INSERT INTO problems(problemName, problemStatement, problemSolution, solutionName) VALUES(?,?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setString(1, name);
            statement.setBlob(2, problemStatement);
            statement.setBlob(3, problemSolution);
            statement.setString(4, solutionName);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addProblem(String name, byte[] problemStatement, byte[] problemSolution, String solutionName) {
        addProblem(name, new ByteArrayInputStream(problemStatement), new ByteArrayInputStream(problemSolution), solutionName);
    }
    public void addProblem(String name, File problemStatement, File problemSolution) {
        try {
            addProblem(name,
                    new FileInputStream(problemStatement),
                    new FileInputStream(problemSolution),
                    problemSolution.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void addProblem(String name, Path problemStatement, Path problemSolution) {
        addProblem(name, problemStatement.toFile(), problemSolution.toFile());
    }
    
    public void deleteTest(int testID) {
    	removeResultsForTest(testID);
    	removeConfigForTest(testID);
    	
        String sqlDelete = 
        		"DELETE FROM tests WHERE testID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, testID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteTest(int problemID, String testName) {
    	int testID = getTestID(problemID, testName);
    	deleteTest(testID);
    }
    public void deleteTest(String problemName, String testName) {
    	int testID = getTestID(problemName, testName);
    	deleteTest(testID);
    }
    
    public void deleteProblem(int problemID) {
    	removeTestsForProblem(problemID);
    	deleteSubmissionsForProblem(problemID);
    	
        String sqlDelete = 
        		"DELETE FROM problems WHERE problems.problemID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, problemID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteProblem(String problemName) {
        int problemID = getProblemID(problemName);
        deleteProblem(problemID);
    }
    
    public void removeResultsForTest(int testID) {
        String sqlDelete =
                "DELETE FROM results WHERE testID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, testID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeResultsForProblem(int problemID) {
        String sqlDelete =
                "DELETE results FROM results JOIN tests ON results.testID = tests.testID" +
                " WHERE tests.problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, problemID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeResultsForProblem(String problemName) {
    	int problemID = getProblemID(problemName);
    	removeResultsForProblem(problemID);
    }

    public void removeTestsForProblem(int problemID) {
    	removeResultsForProblem(problemID);
    	removeTestConfigsForProblem(problemID);
    	
        String sqlDelete = "DELETE FROM tests WHERE tests.problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, problemID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeTestsForProblem(String problemName) {
    	int problemID = getProblemID(problemName);
    	removeTestsForProblem(problemID);
    }
    
    public void removeConfigForTest(int testID) {
        String sqlDelete = "DELETE FROM test_config WHERE test_config.testID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, testID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void removeTestConfigsForProblem(int problemID) {
        String sqlDelete = "DELETE test_config FROM tests JOIN test_config ON tests.testID = test_config.testID WHERE tests.problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, problemID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeTestConfigsForProblem(String problemName) {
    	int problemID = getProblemID(problemName);
    	removeTestConfigsForProblem(problemID);
    }
    
    public void removeAllResultsFor(int studentID, int problemID) {
        String sqlDelete =
                "DELETE results FROM results JOIN tests ON results.testID=tests.testID" +
                        " WHERE results.studentID=? AND tests.problemID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, studentID);
            statement.setInt(2, problemID);

            statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeAllResultsFor(String studentName, int problemID) {
    	int studentID = getStudentID(studentName);
    	removeAllResultsFor(studentID, problemID);
    }
    public void removeAllResultsFor(int studentID, String problemName) {
    	int problemID = getStudentID(problemName);
    	removeAllResultsFor(studentID, problemID);
    }
    public void removeAllResultsFor(String studentName, String problemName) {
    	int studentID = getStudentID(studentName);
    	int problemID = getProblemID(problemName);
    	removeAllResultsFor(studentID, problemID);
    }

	public List<StudentData> getAllStudents(int privilegeLevel) {
        List<StudentData> result = new ArrayList<>();
        String sqlSelect = "SELECT studentID, studentName, studentUsername FROM students WHERE privilegeLevel < ?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
        	statement.setInt(1, privilegeLevel);
        	
            ResultSet results = statement.executeQuery();

            while (results.next()) {
                int studentID = results.getInt("studentID");
                String name = results.getString("studentName");
                String userName = results.getString("studentUsername");
                result.add(new StudentData(studentID, name, userName, 0));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
	}

	public Map<String, Test.Result> getResultsFor(int studentID) {
		Map<String, Test.Result> result = new TreeMap<>();
        String sqlSelect = "SELECT problemName, resultValue FROM problems" +
		" LEFT JOIN tests ON problems.problemID=tests.problemID" +
        		" LEFT JOIN (SELECT * FROM results WHERE results.studentID=?) AS studentresults ON studentresults.testID=tests.testID";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
        	statement.setInt(1, studentID);
        	
            ResultSet results = statement.executeQuery();

            while (results.next()) {
                String problemName = results.getString("problemName");
                if (!result.containsKey(problemName)) {
                	result.put(problemName, Test.Result.PASSED);
                }
                int resultValue = results.getInt("resultValue");
                if (results.wasNull()) {
                	if (result.get(problemName) != Test.Result.PROCESSING) {
                		result.replace(problemName, Test.Result.INVALID);
                	}
                } else if (Test.Result.values()[resultValue] == Test.Result.PROCESSING) {
                	result.replace(problemName, Test.Result.PROCESSING);
                } else if (Test.Result.values()[resultValue] != Test.Result.PASSED) {
                	result.replace(problemName, Test.Result.FAILED);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
	}
	public Map<String, Test.Result> getResultsFor(String studentUsername) {
		int studentID = getStudentID(studentUsername);
		return getResultsFor(studentID);
	}
	
	public int authenicate(String username, String password) {
		int privilegeLevel = -1;
		
		byte[] hashedPass = hashPassword(password, getSalt(username));
		
    	if (hashedPass == null) {
    		return privilegeLevel;
    	}
		
        String sqlSelect = "SELECT privilegeLevel FROM students WHERE studentUsername=? AND password=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setString(1, username);
            statement.setBytes(2, hashedPass);

            ResultSet results = statement.executeQuery();
            if (results.next()) {
                assert results.isLast();
                privilegeLevel = results.getInt("privilegeLevel");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return privilegeLevel;
	}
		
	public int getPrivilege(int studentID) {
		int privilegeLevel = -1;
		
        String sqlSelect = "SELECT privilegeLevel FROM students WHERE studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);

            ResultSet results = statement.executeQuery();
            if (results.next()) {
                assert results.isLast();
                privilegeLevel = results.getInt("privilegeLevel");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return privilegeLevel;
	}		
	public int getPrivilege(String username) {
		int studentID = getStudentID(username);
		return getPrivilege(studentID);
	}
	
	private String genResetRequestID() {
		StringBuilder builder = new StringBuilder();
		Random random = new Random();
		while (builder.length() < 36) {
			int rand = random.nextInt(62);
			char nextChar;
			if (rand < 10) {
				nextChar = (char)('0' + rand);
			} else if (rand < 36) {
				rand -= 10;
				nextChar = (char)('a' + rand);
			} else {
				rand -= 36;
				nextChar = (char)('A' + rand);
			}
			builder.append(nextChar);
		}
		return builder.toString();
	}
	
	public boolean haveResetRequestForStudent(int studentID) {
		deleteExpiredRequests();
		
		boolean haveResult = false;
		
        String sqlSelect = "SELECT * FROM password_reset_requests WHERE studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setInt(1, studentID);

            ResultSet results = statement.executeQuery();
            if (results.next()) {
                assert results.isLast();
                haveResult = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return haveResult;
		
	}
	
	public String registerResetRequest(int studentID) {
		deleteExpiredRequests();
		if (haveResetRequestForStudent(studentID)) {
			deleteResetRequestForStudent(studentID);
		}
		
		String resetID = genResetRequestID();
		
        String sqlInsert = "INSERT INTO password_reset_requests(studentID, randomIdentifier, creationTime) VALUES(?,?,NOW());";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setInt(1, studentID);
            statement.setString(2, resetID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
		
		return resetID;
	}
	public String registerResetRequest(String studentName) {
		int studentID = getStudentID(studentName);
		return registerResetRequest(studentID);
	}
	
	public int getStudentIDFromResetRequest(String id) {
		deleteExpiredRequests();
		
		int studentID = -1;
		
		String sqlSelect = "SELECT studentID FROM password_reset_requests WHERE randomIdentifier=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
            statement.setString(1, id);

            ResultSet results = statement.executeQuery();
            if (results.next()) {
                assert results.isLast();
                studentID = results.getInt("studentID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentID;
	}
	
	public void deleteResetRequestForStudent(int studentID) {
		deleteExpiredRequests();
		
        String sqlDelete = "DELETE FROM password_reset_requests WHERE studentID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, studentID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	public void deleteExpiredRequests() {
        String sqlDelete = "DELETE FROM password_reset_requests WHERE creationTime < ADDDATE(NOW(), INTERVAL -30 MINUTE);";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public void deleteSubmissionsForStudent(int studentID) {
        String sqlDelete = "DELETE FROM submissions WHERE studentID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, studentID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	public void deleteSubmissionsForProblem(int problemID) {
        String sqlDelete = "DELETE FROM submissions WHERE problemID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlDelete)) {
            statement.setInt(1, problemID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public void updateSubmission(int problemID, int studentID, InputStream submission) {
        String sqlUpdate = "UPDATE submissions SET submission=? WHERE problemID=? AND studentID=?;";
        try (PreparedStatement statement = connection.prepareStatement(sqlUpdate)) {
            statement.setBlob(1, submission);
            statement.setInt(2, problemID);
            statement.setInt(3, studentID);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public void addSubmission(int problemID, int studentID, InputStream submission) {
		if (getSubmission(problemID, studentID) != null) {
			updateSubmission(problemID, studentID, submission);
			return;
		}
		
        String sqlInsert = "INSERT INTO submissions(problemID, studentID, submission) VALUES(?,?,?);";
        try (PreparedStatement statement = connection.prepareStatement(sqlInsert)) {
            statement.setInt(1, problemID);
            statement.setInt(2, studentID);
            statement.setBlob(3, submission);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	public void addSubmission(String problemName, int studentID, InputStream submission) {
		int problemID = getProblemID(problemName);
		addSubmission(problemID, studentID, submission);
	}
	public void addSubmission(int problemID, String studentUser, InputStream submission) {
		int studentID = getStudentID(studentUser);
		addSubmission(problemID, studentID, submission);
	}
	public void addSubmission(String problemName, String studentUser, InputStream submission) {
		int problemID = getProblemID(problemName);
		int studentID = getStudentID(studentUser);
		addSubmission(problemID, studentID, submission);
	}
	public void addSubmission(int problemID, int studentID, File submission) {
		try {
			addSubmission(problemID, studentID, new FileInputStream(submission));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void addSubmission(String problemName, int studentID, File submission) {
		int problemID = getProblemID(problemName);
		try {
			addSubmission(problemID, studentID, new FileInputStream(submission));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void addSubmission(int problemID, String studentUser, File submission) {
		int studentID = getStudentID(studentUser);
		try {
			addSubmission(problemID, studentID, new FileInputStream(submission));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void addSubmission(String problemName, String studentUser, File submission) {
		int problemID = getProblemID(problemName);
		int studentID = getStudentID(studentUser);
		try {
			addSubmission(problemID, studentID, new FileInputStream(submission));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	public void addSubmission(int problemID, int studentID, byte[] submission) {
		addSubmission(problemID, studentID, new ByteArrayInputStream(submission));
	}
	public void addSubmission(String problemName, int studentID, byte[] submission) {
		int problemID = getProblemID(problemName);
		addSubmission(problemID, studentID, new ByteArrayInputStream(submission));
	}
	public void addSubmission(int problemID, String studentUser, byte[] submission) {
		int studentID = getStudentID(studentUser);
		addSubmission(problemID, studentID, new ByteArrayInputStream(submission));
	}
	public void addSubmission(String problemName, String studentUser, byte[] submission) {
		int problemID = getProblemID(problemName);
		int studentID = getStudentID(studentUser);
		addSubmission(problemID, studentID, new ByteArrayInputStream(submission));
	}
	
	public InputStream getSubmission(int problemID, int studentID) {
		InputStream result = null;
        String sqlSelect = "SELECT submission FROM submissions WHERE problemID=? AND studentID=?";
        try (PreparedStatement statement = connection.prepareStatement(sqlSelect)) {
        	statement.setInt(1, problemID);
        	statement.setInt(2, studentID);
        	
            ResultSet results = statement.executeQuery();

            if (results.next()) {
            	assert results.isLast();
                result = results.getBlob("submission").getBinaryStream();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
	}
	public InputStream getSubmission(String problemName, int studentID) {
		int problemID = getProblemID(problemName);
		return getSubmission(problemID, studentID);
	}
	public InputStream getSubmission(int problemID, String studentName) {
		int studentID = getStudentID(studentName);
		return getSubmission(problemID, studentID);
	}
	public InputStream getSubmission(String problemName, String studentName) {
		int problemID = getProblemID(problemName);
		int studentID = getStudentID(studentName);
		return getSubmission(problemID, studentID);
	}
}
