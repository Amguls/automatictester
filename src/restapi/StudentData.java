package restapi;

public class StudentData implements Comparable<StudentData> {
	public int studentID;
    public String realName;
    public String userName;
    public int privilegeLevel;

    public StudentData(int studentID, String realName, String userName, int privilegeLevel) {
        this.studentID = studentID; this.realName = realName; this.userName = userName; this.privilegeLevel = privilegeLevel;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof StudentData))
            return false;

        return ((StudentData)obj).studentID == this.studentID;
    }

	@Override
	public int compareTo(StudentData other) {
		return this.realName.compareTo(other.realName);
	}
}