package server;

import restapi.DatabaseConnection;
import restapi.ResultData;
import restapi.TestData;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.util.List;
import java.util.Map;

@WebServlet("/submission")
@MultipartConfig
public class ViewSubmissionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();
        
		if (!PrivilegeChecker.check(request, 1)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		}

		String problem = request.getParameter("problem");
		String student = request.getParameter("student");
		
		InputStream source = db.getSubmission(problem, student);
		String submission = new String(source.readAllBytes());
		
		request.setAttribute("submission", submission);
		
		List<TestData> tests = db.getAllTests(problem);
		Map<Integer, ResultData> results = db.getTestResults(student, problem);
		
		request.setAttribute("tests", tests);
		request.setAttribute("results", results);

        request.getRequestDispatcher("/WEB-INF/jsp/viewSubmissionPage.jsp").forward(request,response);
    }
}
