package server;

import restapi.DatabaseConnection;
import restapi.ProblemData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/problemList")
public class ProblemListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

        List<ProblemData> problems = db.getAllProblems();

        List<String> names = problems.stream()
                .map(data -> data.name).collect(Collectors.toList());
        
        request.setAttribute("namesList", names);

        request.getRequestDispatcher("/WEB-INF/jsp/problemList.jsp").forward(request,response);
    }
}
