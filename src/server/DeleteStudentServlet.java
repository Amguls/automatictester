package server;

import restapi.DatabaseConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet("/deleteStudent")
@MultipartConfig
public class DeleteStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DatabaseConnection db = DatabaseConnection.getInstance();
		
		String studentUsername = request.getParameter("studentUsername");
		
		if (studentUsername == null || studentUsername.length() == 0 || db.getStudentID(studentUsername) < 0) {
			// TODO:
		}

		int priv = db.getPrivilege(studentUsername);
		
		// Do not allow the administrator account to delete itself.
		if (!PrivilegeChecker.check(request, priv + 1, /*allowAdmin=*/false)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		} else {
			db.deleteStudent(studentUsername);
			response.sendRedirect("/AutomaticTester/allResults");
		}
	}
}
