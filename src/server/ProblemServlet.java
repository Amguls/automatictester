package server;

import restapi.DatabaseConnection;
import restapi.ProblemData;
import restapi.TestConfig;
import restapi.TestData;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/problems")
@MultipartConfig
public class ProblemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

        String requestedProblemStatement = request.getParameter("problem");

        if (requestedProblemStatement != null) {
            ProblemData selectedProblem = db.getProblemData(requestedProblemStatement);
            String statement = new String(selectedProblem.statement.readAllBytes());
            request.setAttribute("problemStatement", statement);

            String requestedTest = request.getParameter("test");

            boolean canViewTest = true;
            
            if (requestedTest != null) {
            	if (db.getTest(requestedProblemStatement, requestedTest) != null) {
            		TestConfig testConfig = db.getTestConfig(requestedProblemStatement, requestedTest);
            		if (testConfig != null && testConfig.isHidden && !PrivilegeChecker.check(request,1)) {
            			canViewTest = false;
            		}
            	} else {
            		canViewTest = false;
            	}
            } else {
            	canViewTest = false;
            }
            
            if (canViewTest) {
                TestData selectedTest = db.getTest(requestedProblemStatement, requestedTest);
                TestConfig testConfig = db.getTestConfig(requestedProblemStatement, requestedTest);
                
                request.setAttribute("isHidden", testConfig.isHidden);
                
                String testInput = new String(selectedTest.input.readAllBytes());
                request.setAttribute("testInput", testInput);

                String testOutput = new String(selectedTest.output.readAllBytes());

                request.setAttribute("testOutput", testOutput);
                request.getRequestDispatcher("/WEB-INF/jsp/testPage.jsp").forward(request,response);
            } else {
                List<TestData> tests = db.getAllTests(requestedProblemStatement);
                if (!PrivilegeChecker.check(request, 1)) {
                	tests = tests.stream()
                			.filter(data -> {
                				TestConfig config = db.getTestConfig(data.testID);
                				return (config == null || !config.isHidden);
                			}).collect(Collectors.toList());
                }
                List<String> testNames = tests.stream()
                        .map(data -> data.name).collect(Collectors.toList());
                request.setAttribute("testList", testNames);
                request.getRequestDispatcher("/WEB-INF/jsp/problemPage.jsp").forward(request,response);
            }
        } else {
            List<ProblemData> problems = db.getAllProblems();

            List<String> names = problems.stream()
                    .map(data -> data.name).collect(Collectors.toList());
            
            request.setAttribute("namesList", names);

            request.getRequestDispatcher("/WEB-INF/jsp/allProblems.jsp").forward(request,response);
        }
    }
}
