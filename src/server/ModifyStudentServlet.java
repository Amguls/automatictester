package server;

import restapi.DatabaseConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet("/modifyStudent")
@MultipartConfig
public class ModifyStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DatabaseConnection db = DatabaseConnection.getInstance();
		
		String studentUsername = request.getParameter("studentUsername");
		String password = request.getParameter("password");
		
		boolean valid = true;
		if (studentUsername == null || studentUsername.length() == 0 || db.getStudentID(studentUsername) < 0) {
			valid = false;
		}
		if (password == null || password.length() == 0) {
			valid = false;
		}
		
		if (valid) {
			int priv = db.getPrivilege(studentUsername);
			
			if (!PrivilegeChecker.check(request, priv + 1)) {
	            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
				return;
			}
			
			db.updateStudent(studentUsername, password);
		}
		
		response.sendRedirect("/AutomaticTester/allResults");
	}
}
