package server;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import restapi.DatabaseConnection;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/WEB-INF/jsp/loginPage.jsp").forward(request,response);
    }
	

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DatabaseConnection db = DatabaseConnection.getInstance();
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		int privilegeLevel = db.authenicate(username, password);
		
		if (privilegeLevel >= 0) {
			HttpSession session = request.getSession();
			session.setAttribute("privilege", privilegeLevel);
			session.setAttribute("username", username);
			session.setAttribute("loggedName", db.getStudentRealName(username));
			
	        response.sendRedirect("/AutomaticTester/");
		} else {
			request.getRequestDispatcher("/WEB-INF/jsp/loginPage.jsp").forward(request,response);
		}
    }
}
