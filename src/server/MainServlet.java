package server;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/index.html")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
        String loggedName = (String)session.getAttribute("loggedName");
        
		request.setAttribute("loggedName", loggedName);
        request.getRequestDispatcher("/WEB-INF/jsp/mainPage.jsp").forward(request,response);
    }
}
