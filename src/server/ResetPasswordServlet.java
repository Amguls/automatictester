package server;

import restapi.DatabaseConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;

@WebServlet("/reset")
@MultipartConfig
public class ResetPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

        String id = request.getParameter("id");

        if (id == null) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
            return;
        }
        
        int studentID = db.getStudentIDFromResetRequest(id);
        
        if (studentID < 0) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
            return;
        }
        
        String name = db.getStudentRealName(studentID);
        
        request.setAttribute("studentName", name);

        request.getRequestDispatcher("/WEB-INF/jsp/resetPage.jsp").forward(request,response);
    }

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

        String id = request.getParameter("id");

        if (id == null) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
            return;
        }
        
        int studentID = db.getStudentIDFromResetRequest(id);
        
        if (studentID < 0) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
            return;
        }
        
        String newPassword = request.getParameter("newPassword");
        
        if (newPassword == null || newPassword.length() == 0) {
        	response.sendRedirect(request.getRequestURI());
        	return;
        }
        
        db.updateStudent(studentID, newPassword);
        db.deleteResetRequestForStudent(studentID);
        
    	response.sendRedirect("/AutomaticTester/login");
    }
}
