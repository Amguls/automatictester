package server;

import restapi.DatabaseConnection;
import restapi.ResultData;
import restapi.TestConfig;
import restapi.TestData;
import tester.Test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.*;
import java.util.List;
import java.util.Map;

@WebServlet("/results.xml")
public class ResultsXMLSerlvet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out = response.getWriter();
		DatabaseConnection db = DatabaseConnection.getInstance();

		if (!PrivilegeChecker.isLoggedIn(request)) {
            response.sendRedirect("/AutomaticTester/login");
			return;
		}

		String problemName = request.getParameter("problem");
		String studentName = (String)request.getSession().getAttribute("username");
		
		if (problemName == null || db.getProblemID(problemName) == -1) {
			// TODO:
		}
		if (studentName == null || db.getStudentID(studentName) == -1) {
			// TODO:
		}
		
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();
			Element root = document.createElement("results");
			document.appendChild(root);
		
			List<TestData> tests = db.getAllTests(problemName);	
			Map<Integer, ResultData> results = db.getTestResults(studentName, problemName);
		
			for (int i = 0; i < tests.size(); ++i) {
				Element status = document.createElement("testStatus");
				root.appendChild(status);
				Element testID = document.createElement("testID");
				testID.appendChild(document.createTextNode(""+tests.get(i).testID));
				status.appendChild(testID);
				
				if (!results.containsKey(tests.get(i).testID)) {
					Element result = document.createElement("result");
					result.appendChild(document.createTextNode("0"));
					status.appendChild(result);

					Element resultString = document.createElement("resultString");
					resultString.appendChild(document.createTextNode("Waiting..."));
					status.appendChild(resultString);
					
					Element message = document.createElement("message");
					message.appendChild(document.createTextNode("Waiting..."));
					status.appendChild(message);
				} else {
					ResultData result = results.get(tests.get(i).testID);
					
					Element resultElement = document.createElement("result");
					if (result.result == Test.Result.PROCESSING) {
						resultElement.appendChild(document.createTextNode("0"));
					} else {
						resultElement.appendChild(document.createTextNode("1"));
					}
					status.appendChild(resultElement);

					Element resultString = document.createElement("resultString");
					switch (result.result) {
					case PASSED:
						resultString.appendChild(document.createTextNode("Pass"));
						break;
					case ERROR:
						resultString.appendChild(document.createTextNode("Runtime error"));
						break;
					case TIMEOUT:
						resultString.appendChild(document.createTextNode("Timed out"));
						break;
					case INVALID:
						resultString.appendChild(document.createTextNode("Internal problem"));
						break;
					case FAILED:
						resultString.appendChild(document.createTextNode("Fail"));
						break;
					case PROCESSING:
						resultString.appendChild(document.createTextNode("Processing..."));
						break;
					}
					status.appendChild(resultString);
				
					TestConfig config = db.getTestConfig(tests.get(i).testID);
					
					if (config != null && config.isHidden) {
						Element message = document.createElement("message");
						message.appendChild(document.createTextNode("[HIDDEN]"));
						status.appendChild(message);
					} else {
						Element message = document.createElement("message");
						message.appendChild(document.createTextNode(result.message));
						status.appendChild(message);
					}
				}
			}
		
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			
			StreamResult streamResult = new StreamResult(out);
			transformer.transform(domSource, streamResult);
		} catch (ParserConfigurationException | TransformerException ignored) {}
    }
}
