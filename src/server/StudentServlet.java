package server;

import restapi.DatabaseConnection;
import restapi.ProblemData;
import restapi.StudentData;
import tester.Test;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.List;
import java.util.Map;

@WebServlet("/students")
@MultipartConfig
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

		if (!PrivilegeChecker.check(request, 1)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		}
		
        List<StudentData> students = db.getAllStudents(PrivilegeChecker.getPrivilege(request));
        List<ProblemData> problems = db.getAllProblems();
        
        List<String> problemNames = new ArrayList<>();
        for (ProblemData data : problems) {
        	problemNames.add(data.name);
        }
        
        Map<StudentData, Map<String, Test.Result> > allResults = new TreeMap<>();
        for (StudentData data : students) {
        	allResults.put(data, db.getResultsFor(data.studentID));
        }
        
        request.setAttribute("problems", problemNames);
        request.setAttribute("results", allResults);

        request.getRequestDispatcher("/WEB-INF/jsp/studentsPage.jsp").forward(request,response);
    }
}
