package server;

import restapi.DatabaseConnection;
import restapi.TestConfig;
import restapi.TestData;
import tester.Test;
import tester.TestBuilder;
import tester.Tester;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/results")
@MultipartConfig
public class ResultsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();
        
		if (!PrivilegeChecker.isLoggedIn(request)) {
            response.sendRedirect("/AutomaticTester/login");
			return;
		}
        
        String problem = request.getParameter("problem");
        
        if (problem == null || db.getProblemID(problem) == -1) {
        	// Problem was not specified or does not exist.
        	// TODO: error page.
        }
        
        request.setAttribute("problemStatement", new String(db.getProblemData(problem).statement.readAllBytes()));

        List<TestData> testData = db.getAllTests(problem);
        testData.stream().map(data -> {
        	TestConfig config = db.getTestConfig(data.testID);
        	if (config != null && config.isHidden) {
        		data.name = "[HIDDEN]";
        	}
        	return data;
        }).collect(Collectors.toList());
        
        request.setAttribute("tests", testData);
        
        request.getRequestDispatcher("/WEB-INF/jsp/resultsPage.jsp").forward(request,response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

		if (!PrivilegeChecker.isLoggedIn(request)) {
            response.sendRedirect("/AutomaticTester/login");
			return;
		}
        
        Tester tester = Tester.getInstance();

        String problem = request.getParameter("problem");
        String student = (String)request.getSession().getAttribute("username");
        
        if (problem == null || db.getProblemID(problem) == -1) {
        	// TODO:
        }
        if (student == null || db.getStudentID(student) == -1) {
        	// TODO:
        }
        
        String solution = request.getParameter("source");
        if (solution == null) {
        	// TODO:
        }
        
        String solutionName = request.getParameter("fileName");
        if (solutionName == null || solutionName.length() == 0) {
        	solutionName = "solution";
        }
        String solutionLanguage = request.getParameter("solutionType");
        if (solutionLanguage == null) {
        	// TODO:
        }
        if (!(solutionLanguage.equals("python") || solutionLanguage.equals("java"))) {
        	// TODO:
        }
        
        db.removeAllResultsFor(student, problem);

        List<TestData> testData = db.getAllTests(problem);
        
        Test.Language language = solutionLanguage.equals("python")
                ? Test.Language.PYTHON
                : Test.Language.JAVA;
        String extension = solutionLanguage.equals("python")
                ? ".py"
                : ".java";

        File tempDir = Files.createTempDirectory(student + System.currentTimeMillis()).toFile();
        File sourceCode = Files.createFile(Paths.get(tempDir.getAbsolutePath(),
        		solutionName + extension)).toFile();

        FileOutputStream fos = new FileOutputStream(sourceCode);
        fos.write(solution.getBytes());
        fos.close();
        
        InputStream checker = db.getProblemChecker(problem);
        File checkerFile = null;
        if (checker != null) {
        	checkerFile = Files.createTempFile(tempDir.toPath(), "checker_" + student, ".py").toFile();
        	fos = new FileOutputStream(checkerFile);
            fos.write(checker.readAllBytes());
            fos.close();
        }

        for (TestData data : testData) {
            File savedInput = Files.createTempFile(tempDir.toPath(), "input_" + data.name, ".txt").toFile();
            File savedOutput = Files.createTempFile(tempDir.toPath(), "output_" + data.name, ".txt").toFile();

            fos = new FileOutputStream(savedInput);
            fos.write(data.input.readAllBytes());
            fos.close();
            fos = new FileOutputStream(savedOutput);
            fos.write(data.output.readAllBytes());
            fos.close();

            TestBuilder testBuilder = new TestBuilder()
                    .setInputFile(savedInput)
                    .setOutputFile(savedOutput)
                    .setSourceFile(sourceCode)
                    .setLanguage(language)
                    .setTimeLimit(5000)
                    .setName(data.name)
                    .setProblemName(problem)
                    .setStudentName(student)
                    .setCheckerSourceFile(checkerFile)
                    .setDeleteFiles();
            
            Test test = testBuilder.build();

            tester.queueTest(test);
        }

        response.sendRedirect("/AutomaticTester/results?problem="+problem);
    }
}
