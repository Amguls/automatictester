package server;

import restapi.DatabaseConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet("/deleteTest")
@MultipartConfig
public class DeleteTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();
        
		if (!PrivilegeChecker.check(request, 1)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		}

        String problem = request.getParameter("problem");
        String test = request.getParameter("test");
        
        if (problem != null && test != null) {
            db.deleteTest(problem, test);
        }
        
        if (problem != null) {
        	response.sendRedirect("/AutomaticTester/problems?problem="+problem);
        }
    }
}
