package server;

import restapi.DatabaseConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet("/createStudent")
@MultipartConfig
public class CreateStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DatabaseConnection db = DatabaseConnection.getInstance();
        
		if (!PrivilegeChecker.check(request, 1)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		}
		
		String studentName = request.getParameter("studentName");
		String studentUsername = request.getParameter("studentUsername");
		String password = request.getParameter("password");
		String privilege = request.getParameter("isPrivileged");
		String email = request.getParameter("email");
		boolean isPrivileged = (privilege != null && privilege.equals("true"));
		
		if (isPrivileged) {
			if (!PrivilegeChecker.check(request, 2)) {
	            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
				return;
			}
		}
		
		boolean valid = true;
		if (studentName == null || studentName.length() == 0) {
			valid = false;
		}
		if (studentUsername == null || studentUsername.length() == 0 || db.getStudentID(studentUsername) >= 0) {
			valid = false;
		}
		if (password == null || password.length() == 0) {
			valid = false;
		}
		if (email == null || email.length() == 0) {
			valid = false;
		}
		
		if (valid) {
			db.addStudent(studentUsername, studentName, password, email, isPrivileged);
		}
		
		response.sendRedirect("/AutomaticTester/allResults");
	}
}
