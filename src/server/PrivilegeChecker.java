package server;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class PrivilegeChecker {
	public static boolean isLoggedIn(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (session.getAttribute("privilege") != null);
	}
	
	public static boolean check(HttpServletRequest request, int requiredPrivilege) {
		return check(request, requiredPrivilege, true);
	}
	public static boolean check(HttpServletRequest request, int requiredPrivilege, boolean allowAdmin) {
		HttpSession session = request.getSession();
		Integer privilege = (Integer)session.getAttribute("privilege");
		if (privilege != null && privilege == 999 && allowAdmin) {
			return true;
		}
		return (privilege != null && privilege >= requiredPrivilege);
	}
	
	public static int getPrivilege(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Integer privilege = (Integer)session.getAttribute("privilege");
		if (privilege == null) {
			privilege = -1;
		}
		return privilege;
	}
}
