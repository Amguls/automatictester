package server;

import tester.Test;
import tester.TestBuilder;
import tester.Tester;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import restapi.DatabaseConnection;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@WebServlet("/run")
@MultipartConfig
public class ExecuteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		String input = request.getParameter("input");
		input = input.replaceAll("\\r","");
		String program = request.getParameter("source");
		
		String fileName;
		
		if (program != null && program.length() > 0) {
			fileName = request.getParameter("fileName");
			if (fileName == null || fileName.length() == 0) {
				fileName = "solution";
			}
			
			String type = request.getParameter("type");
			fileName += type.equals("python")
					? ".py"
					: ".java";
		} else {
			DatabaseConnection db = DatabaseConnection.getInstance();
			
			String problem = request.getParameter("problem");
			
			InputStream solution = db.getProblemSolution(problem);
			program = new String(solution.readAllBytes());
			solution.close();
			
			fileName = db.getSolutionName(problem);
		}
		

		Test.Language lang = fileName.endsWith(".py")
				? Test.Language.PYTHON
				: Test.Language.JAVA;

		Tester tester = Tester.getInstance();
		
		File tempDir = Files.createTempDirectory(Long.toString(System.currentTimeMillis())).toFile();
        File sourceCode = Files.createFile(Paths.get(tempDir.getAbsolutePath(),
        		fileName)).toFile();
        
        File inputFile = Files.createFile(Paths.get(tempDir.getAbsolutePath(),
        		"input.txt")).toFile();
        File outputFile = Files.createFile(Paths.get(tempDir.getAbsolutePath(),
        		"output.txt")).toFile();

        FileOutputStream fos = new FileOutputStream(sourceCode);
        fos.write(program.getBytes());
        fos.close();
        
        fos = new FileOutputStream(inputFile);
        fos.write(input.getBytes());
        fos.close();
		
		Test test = new TestBuilder()
				.setLanguage(lang)
				.setInputFile(inputFile)
				.setOutputFile(outputFile)
				.setSourceFile(sourceCode)
				.build();
		
		tester.queueExecute(test);
		
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();
			Element root = document.createElement("executionResult");
			document.appendChild(root);
			
			// Need to wait for the test to complete.
			tester.waitForTestCompletion(test);
			
			Test.Result result = test.getResult();
			
			InputStream fis = new FileInputStream(test.getOutputFile());
			String programOutput = new String(fis.readAllBytes());
			fis.close();
			
			test.deleteFiles();
			
			Element status = document.createElement("status");
			root.appendChild(status);
			if (result == Test.Result.PASSED) {
				status.appendChild(document.createTextNode("0"));
			} else if (result == Test.Result.TIMEOUT) {
				status.appendChild(document.createTextNode("-1"));
			} else {
				status.appendChild(document.createTextNode("-2"));
			}
			Element testOutput = document.createElement("output");
			root.appendChild(testOutput);
			testOutput.appendChild(document.createTextNode(programOutput));
			
			Element message = document.createElement("message");
			root.appendChild(message);
			message.appendChild(document.createTextNode(test.getMessage()));
			
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
			
			StreamResult streamResult = new StreamResult(out);
			transformer.transform(domSource, streamResult);
		} catch (ParserConfigurationException | TransformerException ignored) {}
    }
}
