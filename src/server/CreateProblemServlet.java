package server;

import restapi.DatabaseConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet("/createProblem")
@MultipartConfig
public class CreateProblemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();
        
		if (!PrivilegeChecker.check(request, 1)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		}

        String problemName = request.getParameter("problemName");
        String statement = request.getParameter("problemStatement");
        InputStream stream = new ByteArrayInputStream(statement.getBytes());

        String solution = request.getParameter("solution");
        InputStream source = new ByteArrayInputStream(solution.getBytes());
            
        String checker = request.getParameter("checker");

        String fileName = request.getParameter("fileName");
        if (fileName == null || fileName.length() == 0) {
        	fileName = "solution";
        }
        String solutionType = request.getParameter("solutionType");
        
        String solutionExt = solutionType.equals("python")
        		? ".py"
        		: ".java";

        if (checker == null || checker.length() == 0) {
          	db.addProblem(problemName, stream, source, fileName + solutionExt);
        } else {
         	db.addProblem(problemName, stream, source, fileName + solutionExt, new ByteArrayInputStream(checker.getBytes()));
        }

        response.sendRedirect("/AutomaticTester/problemList");
    }
}
