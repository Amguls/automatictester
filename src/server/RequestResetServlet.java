package server;

import restapi.DatabaseConnection;

/*
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
*/
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
/*
import java.util.Properties;
*/

@WebServlet("/requestReset")
@MultipartConfig
public class RequestResetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/WEB-INF/jsp/requestResetPage.jsp").forward(request,response);
    }

	private void sendResetEmail(String email, String resetURL) {
		// TODO: get this working properly - postfix mailhost is not able to connect.
		/*
		Properties prop = System.getProperties();
		prop.setProperty("mail.smtp.host", "localhost");
		Session session = Session.getDefaultInstance(prop);
		
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("automatictester@fettes.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress("rowanberrylee@gmail.com"));
			message.setSubject("Reset Password Link");
			message.setContent("Click <a href='" + resetURL + "'>here</a> to reset your password", "text/html; charset=utf-8");
			
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		*/
	}
	
	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();

        String username = request.getParameter("username");

        if (username == null) {
            response.sendRedirect(request.getRequestURI());
            return;
        }
        
        int studentID = db.getStudentID(username);
        
        if (studentID < 0) {
            response.sendRedirect(request.getRequestURI());
            return;
        }
        
        String requestID = db.registerResetRequest(studentID);
        String email = db.getStudentEmail(studentID);
        
        sendResetEmail(email, "http://localhost:8080/AutomaticTester/reset?id=" + requestID);
        
        // System.out.println("/AutomaticTester/reset?id=" + requestID);
        
    	response.sendRedirect("/AutomaticTester/index.html");
    }
}
