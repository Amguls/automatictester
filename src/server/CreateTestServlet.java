package server;

import restapi.DatabaseConnection;
import restapi.TestConfig;
import tester.Test;
import tester.TestBuilder;
import tester.Tester;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@WebServlet("/createTest")
@MultipartConfig
public class CreateTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();
        
		if (!PrivilegeChecker.check(request, 1)) {
            request.getRequestDispatcher("/WEB-INF/jsp/unauthorised.jsp").forward(request,response);
			return;
		}

        String problem = request.getParameter("problem");
        
        if (db.getProblemID(problem) == -1) {
    		// TODO:
    	}
    	
    	String testHidden = request.getParameter("testHidden");
        String testName = request.getParameter("testName");
        String input = request.getParameter("testInput");

        InputStream solution = db.getProblemSolution(problem);
        InputStream checker = db.getProblemChecker(problem);
        String solutionName = db.getSolutionName(problem);

        Test.Language language = solutionName.endsWith(".py")
                ? Test.Language.PYTHON
                : Test.Language.JAVA;
                
        Tester tester = Tester.getInstance();

        File tempDir = Files.createTempDirectory(testName).toFile();
        tempDir.deleteOnExit();

        File sourceCode = Files.createFile(Paths.get(tempDir.getAbsolutePath(), solutionName)).toFile();
        File savedInput = Files.createTempFile(tempDir.toPath(), "input", ".txt").toFile();
        File savedOutput = Files.createTempFile(tempDir.toPath(), "output", ".txt").toFile();
        File checkerFile = Files.createTempFile(tempDir.toPath(), "checker", ".py").toFile();

        byte[] solutionBytes = solution.readAllBytes();
        FileOutputStream fos = new FileOutputStream(sourceCode);
        fos.write(solutionBytes);
        fos.close();
        
        fos = new FileOutputStream(savedInput);
        fos.write(input.getBytes());
        fos.close();
        
        if (checker != null) {
            byte[] checkerBytes = checker.readAllBytes();
            fos = new FileOutputStream(checkerFile);
            fos.write(checkerBytes);
            fos.close();
        }

        TestBuilder testBuilder = new TestBuilder()
                .setInputFile(savedInput)
                .setOutputFile(savedOutput)
                .setSourceFile(sourceCode)
                .setLanguage(language)
                .setName(testName)
                .setProblemName(problem)
                .setDeleteFiles();
        
        if (testHidden != null && testHidden.equals("true")) {
        	testBuilder.setConfig(new TestConfig(true));
        }
        if (checker != null) {
        	testBuilder.setCheckerSourceFile(checkerFile);
        }
        
        Test test = testBuilder.build();

        tester.createTest(test);
        tester.waitForTestCompletion(test);
        
        response.sendRedirect("/AutomaticTester/testList?problem=" + problem);
    }
}
