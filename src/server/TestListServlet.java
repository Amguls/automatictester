package server;

import restapi.DatabaseConnection;
import restapi.TestConfig;
import restapi.TestData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/testList")
public class TestListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        DatabaseConnection db = DatabaseConnection.getInstance();
        
        String problem = request.getParameter("problem");
        
        List<TestData> tests = db.getAllTests(problem);
        
        if (!PrivilegeChecker.check(request, 1)) {
        	tests = tests.stream()
        			.filter(data -> {
        				TestConfig config = db.getTestConfig(data.testID);
        				return (config == null || !config.isHidden);
        			}).collect(Collectors.toList());
        }
        		
        List<String> testNames = tests.stream()
                .map(data -> data.name).collect(Collectors.toList());
        request.setAttribute("testList", testNames);
        request.getRequestDispatcher("/WEB-INF/jsp/testList.jsp").forward(request,response);
    }
}
