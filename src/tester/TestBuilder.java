package tester;

import java.io.File;

import restapi.TestConfig;

// Helper class that makes creating tests easier and more readable.
public class TestBuilder {

    private Test builtTest;

    public TestBuilder() {
        builtTest = new Test();
    }

    public TestBuilder setTimeLimit(long limit) {
        builtTest.setTimeLimit(limit);
        return this;
    }
    public TestBuilder setInputFile(File path) {
        builtTest.setInputFile(path);
        return this;
    }
    public TestBuilder setOutputFile(File path) {
        builtTest.setOutputFile(path);
        return this;
    }
    public TestBuilder setSourceFile(File path) {
        builtTest.setSourceFile(path);
        return this;
    }
    public TestBuilder setCheckerSourceFile(File path) {
        builtTest.setCheckerSourceFile(path);
        return this;
    }
    public TestBuilder setName(String name) {
        builtTest.setName(name);
        return this;
    }
    public TestBuilder setLanguage(Test.Language language) {
        builtTest.setLanguage(language);
        return this;
    }
    public TestBuilder setStudentName(String name) {
        builtTest.setStudentName(name);
        return this;
    }
    public TestBuilder setProblemName(String name) {
        builtTest.setProblemName(name);
        return this;
    }
    public TestBuilder setConfig(TestConfig config) {
        builtTest.setConfig(config);
        return this;
    }
    public TestBuilder setDeleteFiles() {
        builtTest.setDeleteFiles();
        return this;
    }

    public Test build() {
        return builtTest;
    }
}
