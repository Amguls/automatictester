package tester;

import java.io.File;

import restapi.TestConfig;

// Represents a test to be executed on the server.
public class Test {
	public enum Language {
        PYTHON,
        JAVA
    }

    private File inputFile;
    private File outputFile;
    private File sourceFile;
    
    // Checker uses null as a value to indicate no checker us to be used.
    private File checkerSourceFile = null;
    private String name;
    private Language language;
    private String studentName;
    private String problemName;
    private TestConfig config;
    private boolean complete = false;
    private boolean deleteFilesOnCompletion = false;

    // This is far more time than any reasonable process would require. This is expressing in milliseconds.
    private long timeLimit = 10000;
    
    // A description of the result of the test.
    private String message = "";

    // The possible results of a test.
    public enum Result {
        PASSED(0),
        FAILED(1),
        TIMEOUT(2),
        ERROR(3),
        INVALID(4),
        PROCESSING(-1);

        public final int priority;
        Result(int priority) {
            this.priority = priority;
        }
    };

    private Result result = Result.PASSED;

    public Result getResult() {
        return result;
    }

    // Results have priorities so that if a test is marked as failed, it can never be set as passed in the future.
    public void setResult(Result result) {
        if (result.priority > this.result.priority) {
            this.result = result;
        }
    }
    
    // Getters and setters.
    public boolean isComplete() { return complete; }
    public void setComplete() { complete = true; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    
    public String getStudentName() { return studentName; }
    public void setStudentName(String studentName) { this.studentName = studentName; }
    
    public String getProblemName() { return problemName; }
    public void setProblemName(String problemName) { this.problemName = problemName; }
    
    public boolean hasConfig() { return config != null; }
    public TestConfig getConfig() { return config; }
    public void setConfig(TestConfig config) { this.config = config; }

    public void setInputFile(File path) {
        inputFile = path;
    }
    public void setOutputFile(File path) {
        outputFile = path;
    }
    public void setSourceFile(File path) {
        sourceFile = path;
    }
    public void setCheckerSourceFile(File path) {
        checkerSourceFile = path;
    }
    public boolean hasCheckerFile() { return checkerSourceFile != null; }
   
    public void setLanguage(Language language) {
        this.language = language;
    }
    public Language getLanguage() {
        return language;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

    public File getInputFile() { return inputFile; }
    public File getOutputFile() { return outputFile; }
    public File getSourceFile() { return sourceFile; }
    public File getCheckerSourceFile() { return checkerSourceFile; }

    public void setTimeLimit(long timeLimit) {
        this.timeLimit = timeLimit;
    }
    
    public void setDeleteFiles() {
    	deleteFilesOnCompletion = true;
    }
    
    public void deleteFiles() {
    	assert sourceFile != null;
    	File parentDir = sourceFile.getParentFile();
    	assert parentDir.isDirectory();
    	File[] contents = parentDir.listFiles();
    	if (contents != null) {
    		for (File f : contents) {
    			f.delete();
    		}
    	}
    	parentDir.delete();
    }
    
    public boolean deleteOnComplete() { return deleteFilesOnCompletion; }

    public long getTimeLimit() { return timeLimit; }
}
