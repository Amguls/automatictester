package tester;

import java.util.Queue;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

// Manages adding test jobs to the test queue and creating the threads that consume them.
// Is a singleton.
public class Tester {
    
    private Tester(int numThreads) {
    	testQueue = new ConcurrentLinkedQueue<>();
    	for (int i = 0; i < numThreads; ++i) {
    		testThreads.add(new TestingThread(this, i, testQueue));
    		testThreads.get(i).start();
    	}
    }
    private static Tester instance = new Tester(4);
    
    public static Tester getInstance() {
    	return instance;
    }
 
    private Queue<TestJob> testQueue;
    private List<TestingThread> testThreads = new ArrayList<>();

    private void addToQueue(Test test, TestJob.JobType type) {
    	synchronized (testQueue) {
            testQueue.add(new TestJob(test, type));
            testQueue.notify();
        }
    }
    
    public void createTest(Test test) {
    	addToQueue(test, TestJob.JobType.CREATE);
    }
    
    public void queueTest(Test test) {
    	addToQueue(test, TestJob.JobType.RUN_ALL);
    }
    
    public void queueExecute(Test test) {
    	addToQueue(test, TestJob.JobType.EXECUTE);
    }

    // Waits until the queue is empty.
    public void completeTests() {
        if (!testQueue.isEmpty()) {
            synchronized (testQueue) {
            	synchronized (this) {
            		while (!testQueue.isEmpty()) {
            			try {
            				wait();
            			} catch (InterruptedException e) {
                        	e.printStackTrace();
                    	}
                	}
            	}
            }
        }
    }
    
    // Waits until a particular test has been marked as complete.
    public void waitForTestCompletion(Test test) {
    	if (!test.isComplete()) {
			synchronized (test) {
				while (!test.isComplete()) {
					try {
						test.wait();
					} catch (InterruptedException ignored) {}
				}
			}
		}
    }
    
    // Stops every thread.
    public void stopThreads() {
    	for (TestingThread t : testThreads) {
    		t.setCompleted();
    	}
    	synchronized(testQueue) {
    		testQueue.notifyAll();
    	}
    }
}
