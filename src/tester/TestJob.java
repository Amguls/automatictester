package tester;

// Represents a job on the queue, containing a test and what operation needs to be performed on it.
public class TestJob {
    public enum JobType {
    	RUN_ALL,
    	CREATE,
    	EXECUTE
   	}
    
    public Test test;
    public JobType jobType;
    public TestJob(Test test, JobType type) {
    	this.test = test;
    	this.jobType = type;
    }
}