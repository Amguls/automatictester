package tester;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import restapi.DatabaseConnection;
import restapi.ResultData;
import tester.TestJob;

// A worker thread that manages the processing of test jobs.
class TestingThread extends Thread {
	
	// Tester that initiated this thread.
	private Tester tester; 
	
	public TestingThread(Tester tester, int id, Queue<TestJob> testQueue) {
		this.isolateID = id;
		this.testQueue = testQueue;
		this.tester = tester;
	}

    private Map<Test.Language, String> extensions =
    		Map.of(Test.Language.PYTHON, ".py",
    				Test.Language.JAVA, ".java");
    private Map<Test.Language, String> names =
    		Map.of(Test.Language.PYTHON, "python",
    				Test.Language.JAVA, "java");
	
    private int isolateID;
    private boolean completed = false;
    private ProcessBuilder processBuilder = new ProcessBuilder();
    private Process process;
    private Queue<TestJob> testQueue;

    // Causes this thread to stop.
    public void setCompleted() {
        synchronized (testQueue) {
            completed = true;
            testQueue.notifyAll();
        }
    }
    
    // Begins the process that executes the test.
    private void createProcess(Test test) {
        try {
            process = processBuilder.start(); 
        } catch (IOException e) {
        	test.setMessage("Test failed to start properly - try resubmitting");
            test.setResult(Test.Result.INVALID);
        }
    }

    // Waits for the test process to complete, killing it if it has taken too long.
    private void monitorProcess(Test test) {
        long startTime = System.currentTimeMillis();
        long currentTime = startTime;
        boolean finished = false;
        long timePassed = currentTime - startTime;

        do {
            try {
                finished = process.waitFor(test.getTimeLimit() - timePassed, TimeUnit.MILLISECONDS);
            } catch (InterruptedException ignored) {}
            currentTime = System.currentTimeMillis();
            timePassed = currentTime - startTime;
        } while (!finished && test.getTimeLimit() - timePassed > 0);

        if (!finished) {
        	// Stop the process.
            process.destroy();
            if (process.isAlive()) {
            	// No survivors.
                process.destroyForcibly();
            }
            test.setResult(Test.Result.TIMEOUT);
        }
    }

    // Dump the process output into the test output file.
    // Used when intention is to record program output.
    private void dumpProcessOutput(Test test) {
        if (test.getResult() != Test.Result.PASSED)
            return;

        InputStream processOutput = process.getInputStream();
        try {
            FileOutputStream outputStream = new FileOutputStream(test.getOutputFile());
            byte[] outputBuffer = new byte[1024];
            while (processOutput.available() > 0) {
                int numRead = processOutput.read(outputBuffer);
                outputStream.write(outputBuffer, 0, numRead);
            }
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Checks if anything unexpected has been printed to stderr.
    private void checkError(Test test) {
        // If timed out, cannot read stream.
        if (test.getResult() != Test.Result.PASSED)
            return;

        InputStream errorOutput = process.getErrorStream();
        try {
        	// Isolate produces one line of output about the status of the program.
        	String error = new String(errorOutput.readAllBytes());
        	String[] errorLines = error.lines().toArray(String[]::new);
        	assert errorLines.length > 0;
        	if (errorLines.length > 1) {
            	StringBuilder sb = new StringBuilder();
            	sb.append("Runtime Error encountered:\n");
            	for (int i = 0; i < errorLines.length - 1; ++i) {
            		sb.append(errorLines[i] + "\n");
            	}
            	test.setMessage(sb.toString());
                test.setResult(Test.Result.ERROR);
            } else {
            	// Only line in stderr is from isolate.
           		if (errorLines[0].startsWith("Time")) {
            		test.setMessage("Timed out (is there the possibility of an infinite loop?)");
            		test.setResult(Test.Result.TIMEOUT);
            	}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Tests whether the output from a program should be considered correct, as compared to the sample output.
    private boolean isEquivalent(byte[] correctOutput, byte[] programOutput) {
    	return Arrays.equals(correctOutput, programOutput);
    }
    
    // Decides whether the test has passed or failed based on its output.
    private void compareOutput(Test test) {
        if (test.getResult() != Test.Result.PASSED)
            return;
        
        InputStream processOutput = process.getInputStream();
        try {
            FileInputStream fileInputStream = new FileInputStream(test.getOutputFile());
            // Just check what the checker said.
            if (test.hasCheckerFile()) {
            	BufferedReader reader = new BufferedReader(new InputStreamReader(processOutput));
            	String line = null, last = "";
            	StringBuilder sb = new StringBuilder();
            	sb.append("Program output:\n");
            	while ((line = reader.readLine()) != null) {
            		sb.append(line + "\n");
            		last = line;
            	}
            	reader.close();
            	// Checker must output a final line ending with "OK" for a test to be passed.
            	if (last.endsWith("OK")) {
                    test.setResult(Test.Result.PASSED);
            	} else {
                	test.setMessage(sb.toString());
                    test.setResult(Test.Result.FAILED);
            	}
                processOutput.close();
            	return;
            }
            byte[] correctOutput = fileInputStream.readAllBytes();
            byte[] programOutput = processOutput.readAllBytes();
            if (isEquivalent(correctOutput, programOutput)) {
                test.setResult(Test.Result.PASSED);
            } else {
            	StringBuilder sb = new StringBuilder();
            	sb.append("Expected:\n");
            	sb.append(new String(correctOutput));
            	sb.append("\nFound:\n");
            	sb.append(new String(programOutput));
            	boolean foundError = false;
            	for (int i = 0; !foundError && i < correctOutput.length; ++i) {
            		if (i >= programOutput.length) {
            			sb.append("\nOutput length is less than expected");
            			foundError = true;
            		} else if (correctOutput[i] != programOutput[i]) {
            			sb.append("\nFirst mismatch at character " + (i+1));
            			foundError = true;
            		}
            	}
            	if (!foundError) {
            		assert programOutput.length > correctOutput.length;
            		sb.append("\nOutput length is greater than expected");
            	}
            	test.setMessage(sb.toString());
                test.setResult(Test.Result.FAILED);
            }
            fileInputStream.close();
            processOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Set up the command to be executed for the test to be run.
    private void prepareTest(Test test) {
    	String extension = extensions.get(test.getLanguage());
    	String name = names.get(test.getLanguage());
    	
    	String fileName = test.getSourceFile().getName();
        assert fileName.endsWith(extension);
    	fileName = fileName.substring(0, fileName.length() - extension.length());
        assert !test.getSourceFile().isDirectory();
    	processBuilder.directory(test.getSourceFile().getParentFile());
    	
    	processBuilder.command("sudo",
    			               "/opt/tester.sh",
    			               Integer.toString(isolateID),
    			               name,
    			               fileName,
    			               test.getInputFile().getName());
    	
    	if (test.hasCheckerFile()) {
    		List<String> command = processBuilder.command();
    		command.add(test.getCheckerSourceFile().getAbsolutePath());
    		processBuilder.command(command);
    	}
    }

    @Override
    public void run() {
    	DatabaseConnection db = DatabaseConnection.getInstance();
    	
        while (!completed) {
            synchronized (testQueue) {
                while (testQueue.isEmpty() && !completed) {
                    try {
                    	testQueue.wait();
                    } catch (InterruptedException ignored) {}
                }
            }
            while (!testQueue.isEmpty()) {
                TestJob testJob = testQueue.poll();
                if (testJob == null) {
                	break;
                }
                Test test = testJob.test;
                if (testJob.jobType == TestJob.JobType.RUN_ALL) {
                	db.addResult(test.getStudentName(),
                			     test.getProblemName(),
                			     test.getName(),
                			     new ResultData(Test.Result.PROCESSING, "Waiting..."));
                    db.addSubmission(test.getProblemName(),
           				 test.getStudentName(),
           				 test.getSourceFile());
                }
                prepareTest(test);
                createProcess(test);
                if (testJob.jobType == TestJob.JobType.RUN_ALL) {
                	monitorProcess(test);
                	checkError(test);
                	compareOutput(test);
                	if (test.getResult() == Test.Result.PASSED) {
                		test.setMessage("Success");
                	}
                	// Truncate message if too long.
                	if (test.getMessage().length() >= 1024) {
                		test.setMessage(test.getMessage().substring(0,1024));
                	}
                    db.addResult(test.getStudentName(),
                    		     test.getProblemName(),
                    		     test.getName(),
                    		     new ResultData(test.getResult(), test.getMessage()));
                } else {
            		monitorProcess(test);
            		dumpProcessOutput(test);
                	if (testJob.jobType == TestJob.JobType.CREATE) {
                		try {
                			if (test.hasConfig()) {
                				db.addTest(test.getProblemName(),
                						   test.getName(),
                    				       new FileInputStream(test.getInputFile()),
                    			           new FileInputStream(test.getOutputFile()),
                    				       test.getConfig());
                			} else {
                				db.addTest(test.getProblemName(),
                    			    	   test.getName(),
                    				       new FileInputStream(test.getInputFile()),
                    				       new FileInputStream(test.getOutputFile()));
                			}
                		} catch (FileNotFoundException ignored) {}
                	} else if (testJob.jobType == TestJob.JobType.EXECUTE){
                		checkError(test);
                	} else {
                		// Unreachable.
                		assert false;
                	}
                }
                // Inform anyone waiting on test that it is complete.
                synchronized (test) {
                	test.setComplete();
                	test.notify();
                }
                if (test.deleteOnComplete()) {
                	test.deleteFiles();
                }
            }
            synchronized(testQueue) {
            	if (testQueue.isEmpty()) {
           			synchronized(tester) {
           				tester.notify();
           			}
           		}
            }
        }
    }
}